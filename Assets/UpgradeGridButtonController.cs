﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UpgradeGridButtonController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public delegate void OnButtonHoverEnter();
    public delegate void OnButtonHoverExit();

    public OnButtonHoverEnter onButtonHoverEnter;
    public OnButtonHoverExit onButtonHoverExit;

    public void OnPointerEnter(PointerEventData eventData)
    {
       onButtonHoverEnter?.Invoke();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        onButtonHoverExit?.Invoke();
    }
}
