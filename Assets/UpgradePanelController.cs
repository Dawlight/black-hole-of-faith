﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Monument;
using UnityEngine;
using UnityEngine.UI;
public class UpgradePanelController : Singleton<UpgradePanelController>
{
    [Serializable]
    public struct ButtonSettings
    {
        public Button MaxCitizenUpgradeButton;
        public Button CitizenSpawnRateUpgradeButton;
        public Button SlingMagnitudeUpgradeButton;
        public Button HealButton;

        public Text MaxCitizenUpgradeText;
        public Text CitizenSpawnRateUpgradeText;
        public Text SlingMagnitudeUpgradeText;
        public Text HealText;

        public UpgradeGridButtonController MaxCitizenUpgradeHover;
        public UpgradeGridButtonController CitizenSpawnRateUpgradeHover;
        public UpgradeGridButtonController SlingMagnitudeUpgradeHover;
        public UpgradeGridButtonController HealButtonHover;
    }

    public Text UpgradeNameText;
    public ButtonSettings UpgradeButtonSettings;
    private AudioSource _audioSource;
    private Canvas _canvas;
    private Animator _animator;

    private Animator[] _childAnimators;

    public Animator MaxCitizensAnimator;
    public Animator SpawnRateAnimator;
    public Animator SlingManitudeAnimator;
    public Animator HealAnimator;


    private Button[] _childButtons;


    public bool IsOpen { get; private set; }

    void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        _canvas = GetComponent<Canvas>();
        _animator = GetComponent<Animator>();

        _childButtons = new Button[] {
            UpgradeButtonSettings.MaxCitizenUpgradeButton,
            UpgradeButtonSettings.CitizenSpawnRateUpgradeButton,
            UpgradeButtonSettings.SlingMagnitudeUpgradeButton,
            UpgradeButtonSettings.HealButton
            };

        _childAnimators = new Animator[] {
            MaxCitizensAnimator,
            SpawnRateAnimator,
            SlingManitudeAnimator,
            HealAnimator
            };

        UpgradeButtonSettings.MaxCitizenUpgradeHover.onButtonHoverEnter += OnMaxCitizensEnter;
        UpgradeButtonSettings.MaxCitizenUpgradeHover.onButtonHoverExit += OnMaxCitizensExit;
        UpgradeButtonSettings.CitizenSpawnRateUpgradeHover.onButtonHoverEnter += OnSpawnRateEnter;
        UpgradeButtonSettings.CitizenSpawnRateUpgradeHover.onButtonHoverExit += OnSpawnRateExit;
        UpgradeButtonSettings.SlingMagnitudeUpgradeHover.onButtonHoverEnter += OnSlingManitudeEnter;
        UpgradeButtonSettings.SlingMagnitudeUpgradeHover.onButtonHoverExit += OnSlingMagnitudeExit;
        UpgradeButtonSettings.HealButtonHover.onButtonHoverEnter += OnHealEnter;
        UpgradeButtonSettings.HealButtonHover.onButtonHoverExit += OnHealExit;

        Close();
        _canvas.enabled = false;
    }

    private void OnHealExit() => UpgradeNameText.text = string.Empty;
    private void OnHealEnter() => UpgradeNameText.text = "HEAL MONUMENT";
    private void OnSlingMagnitudeExit() => UpgradeNameText.text = string.Empty;
    private void OnSlingManitudeEnter() => UpgradeNameText.text = "SLING MAGNITUDE";
    private void OnSpawnRateExit() => UpgradeNameText.text = string.Empty;
    private void OnSpawnRateEnter() => UpgradeNameText.text = "SPAWN RATE";
    private void OnMaxCitizensExit() => UpgradeNameText.text = string.Empty;
    private void OnMaxCitizensEnter() => UpgradeNameText.text = "MAX CITIZENS";
    

    public void PlayClickSound()
    {
        _audioSource.clip = AudioBank.Instance.MenuEffects.Click1;
        _audioSource.Play();
    }


    public void Open()
    {
        _canvas.enabled = true;
        var delay = 0f;

        var last = _childAnimators[_childAnimators.Length - 1];
        foreach (var animator in _childAnimators)
        {
            StartCoroutine(SetGridPanelTrigger(animator, "Open", true, delay, animator == last));
            delay += 0.02f;
        }

        IsOpen = true;
    }

    public void Close()
    {
        IsOpen = false;

        var delay = 0f;
        var last = _childAnimators[_childAnimators.Length - 1];
        foreach (var animator in _childAnimators)
        {
            StartCoroutine(SetGridPanelTrigger(animator, "Open", false, delay, animator == last));
            delay += 0.02f;
        }


    }

    public void CloseWithDelay(float delay)
    {
        StartCoroutine(DoCloseWithDelay(delay));
    }

    private IEnumerator DoCloseWithDelay(float delay)
    {
        yield return new WaitForSecondsRealtime(delay);
        Close();
    }

    private IEnumerator SetGridPanelTrigger(Animator animator, string boolName, bool value, float delay, bool isLast)
    {
        yield return new WaitForSecondsRealtime(delay);

        animator.SetBool(boolName, value);

        if (isLast)
        {
            yield return new WaitForSecondsRealtime(delay);
            OnAnimationsComplete(value);
        }


        yield return 0;
    }


    public void OnHealPressed()
    {
        if (IsOpen)
        {
            PlayClickSound();
            Monument.Instance.SetHealth(Monument.Instance.Health + (Monument.Instance.StartingHealth / 3f));

            HealAnimator.SetBool("Selected", true);
            UpgradesController.Instance.onUpgrade?.Invoke();
        }

        IsOpen = false;
    }

    public void OnMaxCitizensUpgradePressed()
    {
        if (IsOpen)
        {
            PlayClickSound();
            UpgradesController.Instance.TryUpgradeMaxCitizens();

            UpgradeButtonSettings.MaxCitizenUpgradeButton.interactable = !UpgradesController.Instance.IsMaxCitizensMaxed();
            UpgradeButtonSettings.MaxCitizenUpgradeText.text = GetNextLevelText(UpgradesController.Instance.MaxCitizensUpgradeLevel, 5);

            MaxCitizensAnimator.SetBool("Selected", true);
        }

        IsOpen = false;
    }


    public void OnAnimationsComplete(bool open)
    {
        _canvas.enabled = open;
    }

    public void OnSpawnRateUpgradePressed()
    {
        if (IsOpen)
        {
            PlayClickSound();
            UpgradesController.Instance.TryUpgradeCitizenSpawnRate();

            UpgradeButtonSettings.CitizenSpawnRateUpgradeButton.interactable = !UpgradesController.Instance.IsSpawnRateMaxed();
            UpgradeButtonSettings.CitizenSpawnRateUpgradeText.text = GetNextLevelText(UpgradesController.Instance.CitizenSpawnRateUpgradeLevel, 5);

            SpawnRateAnimator.SetBool("Selected", true);

        }

        IsOpen = false;
    }

    public void OnSlingMagnitudeUpgradePressed()
    {
        if (IsOpen)
        {
            PlayClickSound();
            UpgradesController.Instance.TryUpgradeSlingMagnitude();

            UpgradeButtonSettings.SlingMagnitudeUpgradeButton.interactable = !UpgradesController.Instance.IsSlingMagnitudeMaxed();
            UpgradeButtonSettings.SlingMagnitudeUpgradeText.text = GetNextLevelText(UpgradesController.Instance.SlingMagnitudeUpgradeLevel, 5);

            SlingManitudeAnimator.SetBool("Selected", true);
        }

        IsOpen = false;
    }

    private string GetNextLevelText(int current, int max)
    {
        if (current >= max)
        {
            return "MAX";
        }

        return "LEVEL " + (current + 1).ToString();
    }
}
