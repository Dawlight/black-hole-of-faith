﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillWithDelay : MonoBehaviour
{
    public float Delay;
    public AudioClip ExplosionSound;
    private AudioSource _audioSource;
    public float RandomPitchMin = 1;
    public float RandomPitchmax = 1;

    void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    void Start()
    {
        StartCoroutine(StartKill(Delay));
    }

    private IEnumerator StartKill(float delay)
    {
        _audioSource.clip = ExplosionSound;
        _audioSource.pitch = Random.Range(RandomPitchMin, RandomPitchmax);
        _audioSource.Play();
        var timeElapsed = 0f;

        while (timeElapsed < delay)
        {
            timeElapsed += Time.deltaTime;

            yield return 0;
        }

        Destroy(gameObject);
    }
}
