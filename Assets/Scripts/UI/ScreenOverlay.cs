﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenOverlay : Singleton<ScreenOverlay>
{
    public ScreenOverlay() { }

    public Text EnemiesKilled;
    public Text CitizensAvailable;
    public Text CitizensKilled;
    public Text State;

    // public Text CitizenSpawnRateUpgradeLevel;
    // public Text MaxCitizenUpgradeLevel;
    // public Text SlingMagnitudeUpgradeLevel;

    private void Start()
    {
        SetEnemiesKilled(0);
        SetCitizensAvailable(0);
        SetCitizensKilled(0);
    }

    // public void SetCitizenSpawnRateUpgradeLevel(int level)
    // {
    //     CitizenSpawnRateUpgradeLevel.text = level.ToString();
    // }

    // public void SetMaxCitizenUpgradeLevel(int level)
    // {
    //     MaxCitizenUpgradeLevel.text = level.ToString();
    // }

    // public void SetSlingMagnitudeUpgradeLevel(int level)
    // {
    //     SlingMagnitudeUpgradeLevel.text = level.ToString();
    // }

    public void SetEnemiesKilled(int enemiesKilled)
    {
        EnemiesKilled.text = $"{enemiesKilled.ToString()}";
    }

    public void SetCitizensAvailable(int citizensAlive)
    {
        CitizensAvailable.text = $"{citizensAlive} / {Game.Instance.MaxCitizens}";
    }

    public void SetCitizensKilled(int citizensKilled)
    {
        CitizensKilled.text = $"{citizensKilled.ToString()}";
    }

    public void SetState(string state)
    {
        State.text = state;
    }

}
