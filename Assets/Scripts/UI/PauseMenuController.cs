﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenuController : Singleton<PauseMenuController>
{
    public Canvas Canvas;
    public Text WavesCleared;

    void Awake()
    {
        WavesCleared.text = string.Empty;
        Canvas = GetComponent<Canvas>();
    }

    public void QuitGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Main Menu", LoadSceneMode.Single);
    }

    public void Open()
    {
        Canvas.enabled = true;
        Cursor.visible = true;
        Time.timeScale = 0;
    }

    public void Close()
    {
        Canvas.enabled = false;
        Cursor.visible = false;
        Time.timeScale = 1;
    }
}
