﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasRendererWorldFollow : MonoBehaviour
{
    public Transform Follow;
    private RectTransform _rectTransform;

    // Start is called before the first frame update
    void Start()
    {
        _rectTransform = GetComponent<RectTransform>();
    }

    void Update() {
        _rectTransform.position = Follow.transform.position;
    }
}
