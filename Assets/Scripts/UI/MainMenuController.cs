﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    public TitleLetterController TitleLetterController;
    public Image FadeImage;

    public MenuState State = MenuState.Default;

    private float _timeElaspedSinceStarting = 0;
    public float FadeTime;
    public float FadeDelay;
    public float StartDelay;

    public void StartGame()
    {
        TitleLetterController.StartDropping();
        _timeElaspedSinceStarting -= FadeDelay;
        State = MenuState.Starting;
    }

    private void DoStart()
    {
        if (_timeElaspedSinceStarting < FadeTime)
        {

            var factor = _timeElaspedSinceStarting / FadeTime;

            FadeImage.color = new Color(FadeImage.color.r, FadeImage.color.g, FadeImage.color.b, factor);


        }
        else if (_timeElaspedSinceStarting > FadeTime + StartDelay)
        {
            State = MenuState.ChangingScenes;
        }

        _timeElaspedSinceStarting += Time.deltaTime;
    }

    public void QuitGame()
    {
        Application.Quit(0);
    }

    public void LoadGameScene()
    {
        SceneManager.LoadScene("Game", LoadSceneMode.Single);
    }

    public void Update()
    {
        switch (State)
        {
            case MenuState.Starting:
                DoStart();
                return;
            case MenuState.ChangingScenes:
                LoadGameScene();
                return;
            default:
                return;
        }
    }

    public enum MenuState
    {
        Default,
        Starting,
        ChangingScenes
    }

}
