﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamagePopupController : MonoBehaviour
{
    [SerializeField] private Text _text = null;
    public Text Text { get { return _text; } }
}
