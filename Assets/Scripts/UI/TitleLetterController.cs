﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.UI
{

    public class TitleLetterController : MonoBehaviour
    {

        public List<Rigidbody2D> Letters;

        public float DropInterval;
        private float _timeElapsed = 0;
        private int _indexToDrop = 0;
        // Start is called before the first frame update
        void Start()
        {
            _indexToDrop = Letters.Count - 1;
        }

        // Update is called once per frame
        void Update()
        {
            // switch (State)
            // {
            //     case TitleLetterControllerState.Dropping:
            //         DoDropLetters();
            //         return;
            //     default:
            //         return;
            // }

        }

        public void StartDropping()
        {
        }
        
        void DoDropLetters()
        {
            while (_timeElapsed > DropInterval)
            {
                _timeElapsed -= DropInterval;

                ReleaseLetter(Mathf.Min(_indexToDrop--, Letters.Count - 1));
            }

            _timeElapsed += Time.deltaTime;
        }

        void ReleaseLetter(int index)
        {
            var letter = Letters[index];
            letter.constraints = RigidbodyConstraints2D.None;
        }

        public enum TitleLetterControllerState
        {
            Default,
            Dropping,
            Finished
        }

    }
}