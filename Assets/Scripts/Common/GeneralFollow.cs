﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralFollow : MonoBehaviour
{
    public Transform FollowTransform;
    public float SmoothTime;
    public float MaxSpeed;

    private Vector3 _currentVelocity = new Vector3();
    void Start()
    {
        transform.parent = null;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.SmoothDamp(transform.position, FollowTransform.position, ref _currentVelocity, SmoothTime, MaxSpeed);
    }
}
