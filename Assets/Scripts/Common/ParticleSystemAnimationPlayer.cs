﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class ParticleSystemAnimationPlayer : MonoBehaviour
{
    
    public List<ParticleSystem> ParticleSystems;


    public void PlaySystem(int index) {
        ParticleSystems[index].Play();
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
