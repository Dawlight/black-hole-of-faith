﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainListener : Singleton<MainListener>
{
    private AudioListener _listener;
    public AudioListener Listener { get { return _listener; } }
    // Start is called before the first frame update

    public void Awake()
    {
        _listener = GetComponent<AudioListener>();
    }
}
