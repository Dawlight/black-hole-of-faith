﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHoleEffectsClipBank : ClipBank
{
    public AudioClip Forming;
    public AudioClip FullyFormed;
    public AudioClip Fading;
    public AudioClip Fling;
    public AudioClip Explode;
    public AudioClip Blobbify;

   
}
