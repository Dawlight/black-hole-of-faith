﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioBank : Singleton<AudioBank>
{
    // Start is called before the first frame update
   
    public BlackHoleEffectsClipBank BlackHoleEffects;
    public MenuEffectsClipBank MenuEffects;

}
