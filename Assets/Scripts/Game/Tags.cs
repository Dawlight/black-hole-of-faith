﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public static class Tags
    {
        public static readonly string Citizen = "Citizen";
        public static readonly string BlackHole = "BlackHole";
        public static readonly string EnemyProjectile = "EnemyProjectile";
        public static readonly string VillageBorder = "VillageBorder";
        public static readonly string MonumentCollider = "MonumentCollider";
    }
}
