﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHoleExplosionController : MonoBehaviour
{
    // Start is called before the first frame update
    public ParticleSystem HumanParticles;
    public ParticleSystem BloodParticles;

    public void SetParticleEmitterShapeRadius(float radius)
    {
        var humanShape = HumanParticles.shape;
        humanShape.radius = radius;

        var bloodShape = BloodParticles.shape;
        bloodShape.radius = radius;
    }

    public void SetParticleStartingSpeedScale(float scale = 1) {
        var humanVelocity = HumanParticles.velocityOverLifetime;
        humanVelocity.x = new ParticleSystem.MinMaxCurve(-1 * scale, 1 * scale);
        humanVelocity.y = new ParticleSystem.MinMaxCurve(4 * scale, 0);
    }

    public void SetHumanParticleAmount(int gibs) {
        var humanEmission = HumanParticles.emission;
        var burst = humanEmission.GetBurst(0);
        burst.count = gibs;
        humanEmission.SetBurst(0, burst);
    }
}
