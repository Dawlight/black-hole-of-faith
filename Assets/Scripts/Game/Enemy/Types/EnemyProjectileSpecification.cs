﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


namespace Assets.Scripts.Enemy
{
    [CreateAssetMenu(fileName = "Projectile", menuName = "Black Hole/Projectile")]
    public class EnemyProjectileSpecification : ScriptableObject
    {
        public float Damage;
        public float StartingSpeed;
        public float Acceleration;
        public float MaxSpeed;
    }
}
