﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Enemy
{
    public class Enemy : MonoBehaviour
    {
        public delegate void OnDeath(Enemy enemy);

        public OnDeath onKilled;

        public float VelocityMax;
        public float StartingHealth;
        public float DistanceFactorMax;
        public Text HealthText;

        public Monument.Monument TargetMonument;
        public SpriteRenderer SpriteRenderer { get { return _spriteRenderer; } }
        public Rigidbody2D Rigidbody2D { get { return _rigidBody2D; } }
        public Animator Animator { get { return _animator; } }
        public EnemyProjectile EnemyProjectile { get { return _enemyProjectile; } }
        public ParticleSystem SmokeCloudParticles { get { return _smokeCloudParticles; } }

        public EnemyLook EnemyLook;

        [SerializeField] private GameObject _deathEffect = null;
        [SerializeField] private SpriteRenderer _spriteRenderer = null;
        [SerializeField] private EnemyProjectile _enemyProjectile = null;
        [SerializeField] private ParticleSystem _smokeCloudParticles = null;

        public EnemyColors EnemyColors;

        private IEnemyState _state;
        private IEnemyState _attackState;

        private Rigidbody2D _rigidBody2D;
        private Animator _animator;

        private float _currentHealth;
        public float CurrentHealth { get { return _currentHealth; } }

        private void Awake()
        {
            _rigidBody2D = GetComponent<Rigidbody2D>();
            _animator = GetComponent<Animator>();
        }

        void Start()
        {
            _state = new ThinkingState(this);
            _attackState = new DefaultAttackState(this, 0.25f, 1f);

            _currentHealth = StartingHealth;
            HealthText.text = ((int)_currentHealth).ToString();
        }

        void Update()
        {
            _state = _state.Update(Time.deltaTime);
            _attackState = _attackState.Update(Time.deltaTime);
        }

        void FixedUpdate()
        {
            _state.FixedUpdate();
            _attackState.FixedUpdate();
        }
        public void Kill()
        {
            onKilled?.Invoke(this);
            Instantiate(_deathEffect, transform.position, Quaternion.identity);
            CameraShaker.Instance.ShakeCamera(0.1f, 80f, 0.1f);
            Destroy(gameObject);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.tag == Tags.BlackHole && _state.GetType() != typeof(DyingState))
            {
                var blackHole = collision.gameObject.GetComponent<Assets.Scripts.BlackHole.BlackHole>();
                var damageTaken = blackHole.Damage;
                _currentHealth -= damageTaken;

                HealthText.text = ((int)_currentHealth).ToString();
                Game.Instance.SpawnDamageText(transform.position, (int)damageTaken);

                if (_currentHealth <= 0)
                    _state = new DyingState(this, 1);
                else
                    Animate("Hurt", 1f);
            }
        }

        public void SetColorGradient(Gradient gradient)
        {
            var colorOverLifeTime = SmokeCloudParticles.colorOverLifetime;
            colorOverLifeTime.color = gradient;
        }

        public void Attack()
        {
            var direction = TargetMonument.HitCollider.transform.position - transform.position;
            Instantiate(EnemyProjectile, transform.position, Quaternion.LookRotation(Vector3.forward, direction));
        }

        public void Animate(string triggerName, float duration)
        {
            StartCoroutine(DoAnimate(triggerName, duration));
        }

        IEnumerator DoAnimate(string triggerName, float duration)
        {
            var timeElasped = 0f;
            _animator.SetTrigger(triggerName);

            while (timeElasped < duration)
            {
                timeElasped += Time.deltaTime;
                yield return 0;
            }

            _animator.ResetTrigger(triggerName);
            _animator.SetTrigger("Idle");
        }
    }

    [Serializable]
    public struct EnemyColors
    {
        public Gradient DefaultColor;
        public Gradient HurtColor;
        public Gradient AttackColor;
        public Gradient DeathColor;
    }
}