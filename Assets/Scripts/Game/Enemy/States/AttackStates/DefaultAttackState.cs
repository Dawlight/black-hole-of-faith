using System.Collections;
using Assets.Scripts.Enemy;
using UnityEngine;

public class DefaultAttackState : EnemyBaseState
{
    private float _attackTime = 0;
    private float _timeSinceLastAttack = 0;
    private float _chargeTime = 0;

    public DefaultAttackState(Enemy enemy, float attackRate, float chargeTime) : base(enemy)
    {
        _attackTime = 1f / attackRate;
        _chargeTime = chargeTime;
    }

    public override void FixedUpdate() { }

    public override IEnemyState Update(float deltaTime)
    {
        if (_enemy.CurrentHealth > 0)
        {
            UpdateAttack();
        }
        return this;
    }


    public void UpdateAttack()
    {
        while (_timeSinceLastAttack > _attackTime)
        {
            ChargeAttack(_chargeTime);
            _timeSinceLastAttack -= _attackTime;

        }

        _timeSinceLastAttack += Time.deltaTime;
    }

    void ChargeAttack(float chargeTime)
    {
        _enemy.StartCoroutine(DoAttack(chargeTime));
    }

    IEnumerator DoAttack(float chargeTime)
    {
        var smokeCloudMain = _enemy.SmokeCloudParticles.main;
        var originalColor = smokeCloudMain.startColor.color;
        var timeElapsed = 0f;
        _enemy.Animate("Attack", chargeTime);

        while (timeElapsed < chargeTime)
        {
            var factor = timeElapsed / chargeTime;
            smokeCloudMain.startColor = Color.Lerp(Color.white, Color.red, factor);
            timeElapsed += Time.deltaTime;

            yield return 0;
        }

        smokeCloudMain.startColor = originalColor;
        _enemy.Attack();

    }


}