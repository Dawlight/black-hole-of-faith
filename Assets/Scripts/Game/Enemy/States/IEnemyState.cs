﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Enemy
{
    public interface IEnemyState
    {
        IEnemyState Update(float deltaTime);
        void FixedUpdate();

    }
}
