﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Enemy
{
    public abstract class EnemyBaseState : IEnemyState
    {
        protected Enemy _enemy;

        public EnemyBaseState(Enemy enemy)
        {
            _enemy = enemy;
        }

        public abstract void FixedUpdate();
        public abstract IEnemyState Update(float deltaTime);


    }
}
