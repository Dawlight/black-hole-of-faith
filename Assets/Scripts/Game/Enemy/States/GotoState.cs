﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Enemy
{
    class GotoState : EnemyBaseState
    {

        private Vector3 _approachPosition;
        private float _approachDistanceThreshold;

        public GotoState(Enemy enemy, Vector3 approachPosition, float approachDistanceThreshold) : base(enemy)
        {

            _approachPosition = approachPosition;
            _approachDistanceThreshold = approachDistanceThreshold;
        }

        public override void FixedUpdate()
        {
            if (IsFinished() == false)
            {
                DoApproach();
            }
        }

        public override IEnemyState Update(float deltaTime)
        {
            return this;
        }

        private void DoApproach()
        {
            var targetVector = _approachPosition - _enemy.transform.position;
            var direction = targetVector.normalized;

            var currentVelocity = _enemy.Rigidbody2D.velocity;

            _enemy.Rigidbody2D.velocity = Vector3.ClampMagnitude(currentVelocity, 1);
            _enemy.Rigidbody2D.AddForce(targetVector * Time.fixedDeltaTime * 100f, ForceMode2D.Force);
        }

        private bool IsFinished()
        {
            var distance = Vector3.Distance(_enemy.transform.position, _approachPosition);

            if (distance <= _approachDistanceThreshold)
                return true;
            return false;
        }
    }
}
