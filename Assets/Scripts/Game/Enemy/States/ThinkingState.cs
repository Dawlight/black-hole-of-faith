﻿using UnityEngine;

namespace Assets.Scripts.Enemy
{
    class ThinkingState : EnemyBaseState
    {
        public ThinkingState(Enemy enemy) : base(enemy)
        {
        }

        public override IEnemyState Update(float deltaTime)
        {
            if (HasTargetMonument() == false)
            {

                var monument = FindTargetMonument();
                _enemy.TargetMonument = monument;
            }
            else
            {
                var targetPoint = FindHoverPoint();
                return new GotoState(_enemy, targetPoint, 1);
            }

            return this;
        }

        private Vector3 FindHoverPoint()
        {
            var rayDirection = _enemy.TargetMonument.transform.position - _enemy.transform.position;

            var hitInfo = Physics2D.Raycast(_enemy.transform.position, rayDirection.normalized, Mathf.Infinity, LayerMask.GetMask("Monument Enemy Zone"));


            var point = hitInfo.point;
            return point;
        }

        private bool HasTargetMonument()
        {
            return _enemy.TargetMonument != null;
        }

        private Monument.Monument FindTargetMonument()
        {
            var monument = Object.FindObjectOfType<Monument.Monument>();
            if (monument != null) return monument;
            return null;
        }

        public override void FixedUpdate() { }
    }
}
