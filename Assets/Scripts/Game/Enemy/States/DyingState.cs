﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Enemy
{
    class DyingState : EnemyBaseState
    {
        private float _dyingTime;
        private Vector3 _originalScale;


        public DyingState(Enemy enemy, float dyingTime) : base(enemy)
        {
            _dyingTime = dyingTime;
            enemy.Rigidbody2D.gravityScale = 1;
            enemy.Rigidbody2D.drag = 10;
            _originalScale = enemy.SpriteRenderer.transform.localScale;


            _enemy.StartCoroutine(DoDeath(1f));

        }

        private IEnumerator DoDeath(float deathTime)
        {

            var smokeCloudMain = _enemy.SmokeCloudParticles.main;
            var originalColor = smokeCloudMain.startColor.color;
            var timeElapsed = 0f;
            _enemy.Animate("Death", deathTime);

            while (timeElapsed < deathTime)
            {
                var factor = timeElapsed / deathTime;
                smokeCloudMain.startColor = Color.Lerp(originalColor, Color.gray, factor);
                timeElapsed += Time.deltaTime;

                yield return 0;
            }

            // smokeCloudMain.startColor = originalColor;
            _enemy.Kill();
        }

        public override IEnemyState Update(float deltaTime)
        {
            // if (_elapsedTime > _dyingTime)
            // {
            //     Die();
            //     _enemy.Kill();
            // }

            // _elapsedTime += Time.deltaTime;


            // var lightness = 1 - (_elapsedTime / _dyingTime);


            // _enemy.SpriteRenderer.color = new Color(lightness, lightness, lightness);

            return this;
        }

        public override void FixedUpdate() { }
    }
}
