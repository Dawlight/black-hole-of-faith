﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Enemy
{
    class SelectTargetState : EnemyBaseState
    {
        public SelectTargetState(Enemy enemy) : base(enemy)
        {
        }

        public override void FixedUpdate()
        {
        }



        public override IEnemyState Update(float deltaTime) { return this; }
    }
}
