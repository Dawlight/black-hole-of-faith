﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLook : MonoBehaviour
{

    public Transform LookTarget;
    public static Vector2 LookMagnitude = new Vector3(0.3f, 0.5f, 0);
    private Vector3 _originalPosition;

    void Start()
    {
        _originalPosition = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        if (LookTarget != null)
        {
            var lookDirection = Vector3.ClampMagnitude(LookTarget.position - transform.position, 1f);
            var newPosition = Vector3.Scale(_originalPosition + lookDirection, LookMagnitude);
            transform.localPosition = newPosition;
        }
    }
}
