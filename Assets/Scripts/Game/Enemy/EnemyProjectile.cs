﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Enemy
{
    public class EnemyProjectile : MonoBehaviour
    {
        public EnemyProjectileSpecification ProjectileSpecification;
        public GameObject ProjectileExplosion;

        private float _speed = 0;

        void Start()
        {
            _speed = 0;
        }

        void Update()
        {
            transform.position += transform.up * _speed * Time.deltaTime;

            _speed += ProjectileSpecification.Acceleration * Time.deltaTime;
            _speed = Mathf.Min(_speed, ProjectileSpecification.MaxSpeed);
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            var contact = collision.contacts[0];           
            var instance = Instantiate(ProjectileExplosion, transform.position, Quaternion.identity);
            instance.transform.rotation = Quaternion.FromToRotation(instance.transform.up, contact.normal);

            KillWithDelay(1f);
        }

        public void KillWithDelay(float delay)
        {
            


            StartCoroutine(DestroyWithDelay(delay));
        }

        public IEnumerator DestroyWithDelay(float delay)
        {
            var elapsedTime = 0f;

            while (elapsedTime < delay)
            {

                elapsedTime += Time.deltaTime;

                yield return 0;
            }

            Destroy(gameObject);
        }

    }
}