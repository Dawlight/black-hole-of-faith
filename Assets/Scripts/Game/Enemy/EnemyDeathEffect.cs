﻿using UnityEngine;

public class EnemyDeathEffect : MonoBehaviour
{
    [SerializeField] private float _lifetime = 0f;
    private float _timeElapsed = 0;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (_timeElapsed > _lifetime)
        {
            Destroy(gameObject);
        }

        _timeElapsed += Time.deltaTime;
    }
}
