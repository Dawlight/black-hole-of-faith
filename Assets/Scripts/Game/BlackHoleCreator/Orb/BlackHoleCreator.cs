﻿using Assets.Scripts.BlackHole.BlackHoleCreatorState;
using System;
using UnityEngine;

namespace Assets.Scripts.BlackHole
{
    public class BlackHoleCreator : Singleton<BlackHoleCreator>
    {
        public delegate void OnBlackHoleRelease();

        public event OnBlackHoleRelease onBlackHoleRelease;

        public float ClickRadius { get { return _clickRadius; } }
        public float SlingForceMultiplier { get { return _slingForceMultiplier; } }
        public Controls Controls { get { return _controls; } }
        public BlackHole BlackHolePrefab { get { return _blackHolePrefab; } }
        public SpriteRenderer SpriteRenderer { get { return _spriteRenderer; } }

        [SerializeField] private float _clickRadius = 0f;
        [SerializeField, Range(1, 1000)] private float _slingForceMultiplier = 0;
        [SerializeField] private Controls _controls = null;
        [SerializeField] private BlackHole _blackHolePrefab = null;

        private SpriteRenderer _spriteRenderer;
        private IBlackHoleCreatorState _state;

        private void Awake()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        void Start()
        {
            _state = new IdleState(this);
        }

        void Update()
        {
            _state = _state.HandleInput();
            _state = _state.Update(Time.deltaTime);
        }

        public void SetDisabled()
        {
            _state = new DisabledState(this);

        }

        public void SetEnabled()
        {
            _state = new IdleState(this);

        }

        public void ReleaseBlackHole()
        {
            onBlackHoleRelease?.Invoke();

        }

        public void OnDisable()
        {
            onBlackHoleRelease = null;
        }
    }

    [Serializable]
    public class Controls
    {
        public string CreateButton;
        public string SlingButton;
    }


}