﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMouse : MonoBehaviour
{


    void LateUpdate()
    {
        var mouseScreenPosition = Input.mousePosition;
        var mouseWorldPosition = Camera.main.ScreenToWorldPoint(mouseScreenPosition);
        var currentPosition = transform.position;
        var newPosition = new Vector3(mouseWorldPosition.x, mouseWorldPosition.y, currentPosition.z);
        transform.position = newPosition;
    }
}
