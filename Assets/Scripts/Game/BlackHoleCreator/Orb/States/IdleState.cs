﻿using Assets.Scripts.BlackHole.BlackHoleCreatorStates;
using Assets.Scripts.BlackHole.BlackHoleStates;
using UnityEngine;

namespace Assets.Scripts.BlackHole.BlackHoleCreatorState
{
    public class IdleState : BaseState
    {

        public IdleState(BlackHoleCreator creator) : base(creator)
        {
            BlackHoleCreator.SpriteRenderer.color = Color.white;
        }

        public override IBlackHoleCreatorState HandleInput()
        {
            if (Input.GetButton(BlackHoleCreator.Controls.CreateButton) && Game.Instance.LiveBlackHolesCount == 0)
            {
                return new CreateState(BlackHoleCreator);
            }

            if (Input.GetButton(BlackHoleCreator.Controls.SlingButton))
            {
                var nearestHole = FindNearestBlackHole();

                if (nearestHole == null) return this;

                return new SlingDragStartState(BlackHoleCreator, nearestHole);
            }

            return this;
        }

        public override IBlackHoleCreatorState Update(float deltaTime)
        {
            return this;
        }

        private BlackHole FindNearestBlackHole()
        {
            var blackHoles = Object.FindObjectsOfType<BlackHole>();
            var creatorPosition = BlackHoleCreator.transform.root.position;

            BlackHole nearest = null;
            float minimumDistance = Mathf.Infinity;

            foreach (var hole in blackHoles)
            {
                if (hole.State.GetType() != typeof(RestingState)) continue;

                var distance = Vector3.Distance(hole.transform.root.position, creatorPosition);
                if (distance > BlackHoleCreator.ClickRadius) continue;


                if (distance < minimumDistance)
                {
                    minimumDistance = distance;
                    nearest = hole;
                }
            }

            return nearest;
        }
    }
}
