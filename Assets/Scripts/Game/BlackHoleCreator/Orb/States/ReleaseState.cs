﻿using Assets.Scripts.BlackHole.BlackHoleCreatorState;
using UnityEngine;

namespace Assets.Scripts.BlackHole.BlackHoleCreatorStates
{
    public class ReleaseState : BaseState
    {
        public ReleaseState(BlackHoleCreator creator) : base(creator)
        {
            creator.SpriteRenderer.color = Color.blue;
        }

        public override IBlackHoleCreatorState HandleInput()
        {
            return this;
        }

        public override IBlackHoleCreatorState Update(float deltaTime)
        {
            BlackHoleCreator.ReleaseBlackHole();
            return new CooldownState(BlackHoleCreator, 0);
        }
    }
}
