﻿using Assets.Scripts.BlackHole.BlackHoleCreatorState;
using UnityEngine;

namespace Assets.Scripts.BlackHole.BlackHoleCreatorStates
{
    public class CreateState : BaseState
    {
        private float _chargeTime = 0;
        private float maxChargeTime = 0;

        public CreateState(BlackHoleCreator creator) : base(creator)
        {
            creator.SpriteRenderer.color = Color.clear;
        }

        public override IBlackHoleCreatorState HandleInput()
        {
            return this;
        }

        public override IBlackHoleCreatorState Update(float deltaTime)
        {
            if (_chargeTime > maxChargeTime)
            {
                var newInstance = Game.Instance.SpawnNewBlackHole(BlackHoleCreator.BlackHolePrefab, BlackHoleCreator.transform.position);
                newInstance.SetCreator(BlackHoleCreator);

                return new HoldState(BlackHoleCreator);
            }

            _chargeTime += deltaTime;
            return this;
        }
    }
}
