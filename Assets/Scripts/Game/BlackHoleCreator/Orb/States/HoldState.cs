﻿using Assets.Scripts.BlackHole.BlackHoleCreatorState;
using UnityEngine;

namespace Assets.Scripts.BlackHole.BlackHoleCreatorStates
{
    public class HoldState : BaseState
    {
        public HoldState(BlackHoleCreator creator) : base(creator)
        {
            creator.SpriteRenderer.color = Color.clear;
        }

        public override IBlackHoleCreatorState HandleInput()
        {
            if (Input.GetButton(BlackHoleCreator.Controls.CreateButton))
            {
                return this;
            }

            return new ReleaseState(BlackHoleCreator);
        }

        public override IBlackHoleCreatorState Update(float deltaTime)
        {
            return this;
        }
    }
}
