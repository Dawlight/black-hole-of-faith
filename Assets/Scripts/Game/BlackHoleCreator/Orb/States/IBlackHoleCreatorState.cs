﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.BlackHole.BlackHoleCreatorState
{
    public interface IBlackHoleCreatorState
    {
        IBlackHoleCreatorState HandleInput();
        IBlackHoleCreatorState Update(float deltaTime);
    }
}
