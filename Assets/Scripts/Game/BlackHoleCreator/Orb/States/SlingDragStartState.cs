﻿using Assets.Scripts.BlackHole.BlackHoleCreatorState;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.BlackHole.BlackHoleCreatorStates
{
    public class SlingDragStartState : BaseState
    {
        private BlackHole _blackHole;

        public SlingDragStartState(BlackHoleCreator creator, BlackHole blackHole) : base(creator)
        {
            creator.SpriteRenderer.color = Color.white;
            _blackHole = blackHole;

            blackHole.StartDragging(creator.transform, blackHole.transform.position);

            CameraPanner.Instance.State = new CameraAimState(CameraPanner.Instance, blackHole);
        }

        public override IBlackHoleCreatorState HandleInput()
        {
            if (UnityEngine.Input.GetButton(BlackHoleCreator.Controls.SlingButton) == false)
            {
                var slingVector = _blackHole.transform.position - BlackHoleCreator.transform.root.position;

                _blackHole.Sling(slingVector * BlackHoleCreator.SlingForceMultiplier);

                  CameraPanner.Instance.State = new DefaultPanningState(CameraPanner.Instance);
                return new CooldownState(BlackHoleCreator, 0);
            }

            return this;
        }

        public override IBlackHoleCreatorState Update(float deltaTime)
        {
            return this;
        }
    }
}
