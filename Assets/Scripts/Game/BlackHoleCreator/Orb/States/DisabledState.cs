using Assets.Scripts.BlackHole;
using Assets.Scripts.BlackHole.BlackHoleCreatorState;
using UnityEngine;

public class DisabledState : BaseState
{
    public DisabledState(BlackHoleCreator creator) : base(creator)
    {
        creator.SpriteRenderer.color = Color.white;
    }

    public override IBlackHoleCreatorState HandleInput()
    {
        return this;
    }

    public override IBlackHoleCreatorState Update(float deltaTime)
    {
        return this;
    }
}