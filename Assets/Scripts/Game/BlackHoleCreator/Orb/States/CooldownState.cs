﻿using Assets.Scripts.BlackHole.BlackHoleCreatorState;
using UnityEngine;

namespace Assets.Scripts.BlackHole.BlackHoleCreatorStates
{
    public class CooldownState : BlackHoleCreatorState.BaseState
    {
        private float _cooldownTime = 0;
        private float _timeElapsed = 0;

        public CooldownState(BlackHoleCreator creator, float cooldownTime) : base(creator)
        {
            BlackHoleCreator.SpriteRenderer.color = Color.yellow;
            _cooldownTime = cooldownTime;
        }


        public override IBlackHoleCreatorState HandleInput()
        {
            return this;
        }

        public override IBlackHoleCreatorState Update(float deltaTime)
        {
            if (_timeElapsed > _cooldownTime)
            {
                return new IdleState(BlackHoleCreator);
            }

            _timeElapsed += deltaTime;

            return this;
        }
    }
}
