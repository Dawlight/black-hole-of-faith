﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.BlackHole.BlackHoleCreatorState
{
    public abstract class BaseState : IBlackHoleCreatorState
    {


        private BlackHoleCreator _creator;
        public BlackHoleCreator BlackHoleCreator { get { return _creator; } }

        public BaseState(BlackHoleCreator creator)
        {
            _creator = creator;
        }

        abstract public IBlackHoleCreatorState HandleInput();
        abstract public IBlackHoleCreatorState Update(float deltaTime);
    }
}

