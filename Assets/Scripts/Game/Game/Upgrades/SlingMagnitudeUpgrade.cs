public class SlingMagnitudeUpgrade : BaseUpgrade
{
    public int Level;

    public SlingMagnitudeUpgrade(int level)
    {
        Level = level;
    }

    public override void Apply(Game game)
    {
        game.SlingMagnitude = UpgradesController.Instance.Configuration.SlingMagnitude.Levels[Level];
        // ScreenOverlay.Instance.SetSlingMagnitudeUpgradeLevel(Level);
    }
}