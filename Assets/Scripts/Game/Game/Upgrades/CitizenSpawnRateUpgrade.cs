public class CitizenSpawnRateUpgrade : BaseUpgrade
{
    public int Level;

    public CitizenSpawnRateUpgrade(int level)
    {
        Level = level;
    }

    public override void Apply(Game game)
    {
        game.CitizenSpawnRate = UpgradesController.Instance.Configuration.CitizenSpawnRate.Levels[Level];
        // ScreenOverlay.Instance.SetCitizenSpawnRateUpgradeLevel(Level);
    }
}