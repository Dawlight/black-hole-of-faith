public interface IGameUpgrade
{
    void Apply(Game game);
}