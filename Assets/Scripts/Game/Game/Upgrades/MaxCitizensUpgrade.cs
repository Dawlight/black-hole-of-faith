public class MaxCitizensUpgrade : BaseUpgrade
{
    public int Level;

    public MaxCitizensUpgrade(int level)
    {
        Level = level;
    }

    public override void Apply(Game game)
    {
        game.MaxCitizens = UpgradesController.Instance.Configuration.MaxCitizens.Levels[Level];
        // ScreenOverlay.Instance.SetMaxCitizenUpgradeLevel(Level);
    }
}