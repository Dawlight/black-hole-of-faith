public abstract class BaseUpgrade : IGameUpgrade
{
    public abstract void Apply(Game game);
}