﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Enemy;
using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy Wave", menuName = "Black Hole/Enemy Wave")]
public class EnemyWave : ScriptableObject
{
  
    [SerializeField]
    public List<Enemy> Enemies;
}
