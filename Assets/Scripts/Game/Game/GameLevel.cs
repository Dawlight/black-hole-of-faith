﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Level", menuName = "Black Hole/Level")]
public class GameLevel : ScriptableObject
{
    public List<EnemyWave> Waves;
}
