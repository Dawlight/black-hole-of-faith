﻿using Assets.Scripts.BlackHole;
using Assets.Scripts.BlackHole.BlackHoleStates;
using Assets.Scripts.Enemy;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game : Singleton<Game>
{
    public GeneralConfiguration GeneralConfiguration;
    public GameObjectConfiguration GameObjectConfiguration;
    public BlackHoleConfiguration BlackHoleConfiguration;
    public UIConfiguration UIConfiguration;
    private AudioSource _mainAudio;

    public MaxCitizensUpgrade MaxCitizensUpgrade;
    public CitizenSpawnRateUpgrade CitizensSpawnRateUpgrade;
    public SlingMagnitudeUpgrade SlingMagnitudeUpgrade;

    public delegate void OnEnemyKilledGlobal(Enemy enemy);
    public delegate void OnCitizenKilledGlobal(Citizen citizen);

    public OnEnemyKilledGlobal onEnemyKilledGlobal;
    public OnCitizenKilledGlobal onCitizenKilledGlobal;

    public IGameState State { get; private set; }

    public GameLevel Level;
    public int CurrentWaveIndex = 0;
    public GameState GameState;

    public int MaxCitizens { get; set; }
    public float CitizenSpawnRate { get; set; }
    public float SlingMagnitude { get; set; }

    public int EnemiesKilledGlobal { get { return _enemiesKilledGlobal; } }
    public int CitizensKilledGlobal { get { return _citizensKilledGlobal; } }

    /* Spawn Lists */
    public List<Enemy> EnemiesAlive { get; private set; } = new List<Enemy>();
    public List<Citizen> CitizensAlive { get; private set; } = new List<Citizen>();
    public List<BlackHole> BlackHolesActive { get; private set; } = new List<BlackHole>();

    /* Global counters and flags */
    private bool _gameIsFrozen = false;
    private int _enemiesKilledGlobal = 0;
    private int _citizensKilledGlobal = 0;

    private EnemySpawnPoint[] _enemySpawnPoints;
    private CitizenSpawnPoint[] _citizenSpawnPoints;

    /* Let's not count fading or flying black holes. They can live their own lives. */
    public float LiveBlackHolesCount { get { return BlackHolesActive.FindAll(hole => hole.State.GetType() != typeof(ProjectileState) && hole.State.GetType() != typeof(FadeState)).Count; } }

    void Awake()
    {
        _mainAudio = GetComponent<AudioSource>();
        var upgradesController = UpgradesController.Instance;

        MaxCitizensUpgrade = new MaxCitizensUpgrade(upgradesController.MaxCitizensUpgradeLevel);
        CitizensSpawnRateUpgrade = new CitizenSpawnRateUpgrade(upgradesController.CitizenSpawnRateUpgradeLevel);
        SlingMagnitudeUpgrade = new SlingMagnitudeUpgrade(upgradesController.SlingMagnitudeUpgradeLevel);

        MaxCitizensUpgrade.Apply(this);
        CitizensSpawnRateUpgrade.Apply(this);
        SlingMagnitudeUpgrade.Apply(this);

        _enemySpawnPoints = FindObjectsOfType<EnemySpawnPoint>();
        _citizenSpawnPoints = FindObjectsOfType<CitizenSpawnPoint>();

        State = new WaveIntermissionState(this, Game.Instance.GeneralConfiguration.DefaultIntermissionTime);
    }

    void Start()
    {
        CameraZoomer.Instance.TargetSize = CameraZoomer.Instance.MaxCameraSize;
    }

    public void PlaySound(AudioClip clip, float pitch)
    {
        _mainAudio.clip = clip;
        _mainAudio.pitch = pitch;
        _mainAudio.Play();
    }


    void Update()
    {
        State = State.Update(Time.deltaTime);
    }

    public void ApplyUpgrade(IGameUpgrade upgrade)
    {
        upgrade.Apply(this);
    }

    /* Callback Methods START */

    public void OnBlackHoleKilled(BlackHole blackHole)
    {
        BlackHolesActive.Remove(blackHole);
        blackHole.onKilled -= OnBlackHoleKilled;
    }

    private void OnCitizenKilled(Citizen citizen)
    {
        _citizensKilledGlobal++;
        onCitizenKilledGlobal?.Invoke(citizen);

        CitizensAlive.Remove(citizen);

        ScreenOverlay.Instance.SetCitizensAvailable(CitizensAlive.Count);
        ScreenOverlay.Instance.SetCitizensKilled(_citizensKilledGlobal);

        citizen.onKilled -= OnCitizenKilled;
    }

    public void OnEnemyKilled(Enemy enemy)
    {
        _enemiesKilledGlobal++;
        onEnemyKilledGlobal?.Invoke(enemy);

        EnemiesAlive.Remove(enemy);

        ScreenOverlay.Instance.SetEnemiesKilled(_enemiesKilledGlobal);

        enemy.onKilled -= OnEnemyKilled;
    }

    /* Callback Methods END */

    /* Effects START */

    public void Freeze(float duration)
    {
        if (_gameIsFrozen == false)
            StartCoroutine(DoFreeze(duration));
    }

    public void SpawnDamageText(Vector3 spawnPosition, int damageAmount)
    {
        var instance = Instantiate(UIConfiguration.DamagePopupPrefab);
        instance.transform.SetParent(UIConfiguration.MainCanvas.transform);

        var rectTransform = instance.GetComponent<RectTransform>();
        instance.Text.text = damageAmount.ToString();

        Vector3 viewPos = Camera.main.WorldToViewportPoint(spawnPosition + new Vector3(0, 3f, 0));

        rectTransform.anchoredPosition = new Vector3(viewPos.x * UIConfiguration.MainCanvas.pixelRect.width, viewPos.y * UIConfiguration.MainCanvas.pixelRect.height);
    }

    /* Effects END */


    /* Coroutines START */

    private IEnumerator DoFreeze(float duration)
    {
        _gameIsFrozen = true;
        var originalTimeScale = Time.timeScale;
        var timeElapsed = 0f;


        Time.timeScale = 0;

        while (timeElapsed < duration)
        {
            timeElapsed += Time.unscaledDeltaTime;

            yield return 0;
        }

        Time.timeScale = originalTimeScale;
        _gameIsFrozen = false;
    }

    /* Coroutines END */


    /* Spawn Methods START */

    public Enemy SpawnNewEnemy(Enemy enemyPrefab)
    {
        var activeSpawnPointIndex = UnityEngine.Random.Range(0, _enemySpawnPoints.Length);
        var activeSpawnPoint = _enemySpawnPoints[activeSpawnPointIndex];
        var position = activeSpawnPoint.gameObject.transform.position;

        var newEnemy = Instantiate(enemyPrefab, position, Quaternion.identity, GameObjectConfiguration.EnemyContainer);
        newEnemy.onKilled += OnEnemyKilled;

        Instantiate(GameObjectConfiguration.EnemySpawnEffect, position, Quaternion.identity);

        EnemiesAlive.Add(newEnemy);

        return newEnemy;
    }

    public Citizen SpawnNewCitizen()
    {
        var activeSpawnPointIndex = UnityEngine.Random.Range(0, _citizenSpawnPoints.Length);
        var activeSpawnPoint = _citizenSpawnPoints[activeSpawnPointIndex];
        var position = activeSpawnPoint.transform.position;

        var newCitizen = Instantiate(GameObjectConfiguration.CitizenPrefab, position, Quaternion.identity, GameObjectConfiguration.CitizensContainer);
        newCitizen.onKilled += OnCitizenKilled;

        CitizensAlive.Add(newCitizen);

        ScreenOverlay.Instance.SetCitizensAvailable(CitizensAlive.Count);

        return newCitizen;
    }

    public BlackHole SpawnNewBlackHole(BlackHole prefab, Vector3 position)
    {
        var newBlackHole = Instantiate(prefab, position, Quaternion.identity);
        newBlackHole.onKilled += OnBlackHoleKilled;

        newBlackHole.CitizenArea = BlackHoleConfiguration.CitizenArea;
        newBlackHole.AreaWeight = BlackHoleConfiguration.AreaWeight;
        newBlackHole.DamagePerMass = BlackHoleConfiguration.DamagePerMass;

        BlackHolesActive.Add(newBlackHole);

        ScreenOverlay.Instance.SetCitizensAvailable(CitizensAlive.Count);

        return newBlackHole;
    }

    /* Spawn Methods END */
}

[Serializable]
public struct GeneralConfiguration
{
    public int StartingMaxCitizens;
    public float StartingCitizenSpawnRate;
    public float DefaultIntermissionTime;
    public float DefaultWarmupTime;
}

[Serializable]
public struct GameObjectConfiguration
{
    public Transform CitizensContainer;
    public Transform EnemyContainer;
    public Transform BlackHoleRestingTransform;
    public Citizen CitizenPrefab;
    public GameObject EnemySpawnEffect;
}

[Serializable]
public struct BlackHoleConfiguration
{
    [Range(0f, 2f)] public float CitizenArea;
    [Range(0f, 10f)] public float AreaWeight;
    [Range(0f, 1000f)] public float DamagePerMass;
}

[Serializable]
public struct UIConfiguration
{
    public Canvas MainCanvas;
    public DamagePopupController DamagePopupPrefab;
}

public enum GameState
{
    Paused,
    PreSession,
    ActiveSession,
    PostSession,
    Menu
}