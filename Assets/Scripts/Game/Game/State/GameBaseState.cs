using UnityEngine;

public abstract class GameBaseState : IGameState
{
    protected Game _game;

    public GameBaseState(Game game)
    {
        _game = game;
        ScreenOverlay.Instance.SetState(this.StateName);
    }

    public abstract string StateName { get; }


    public abstract IGameState Update(float deltaTime);
}