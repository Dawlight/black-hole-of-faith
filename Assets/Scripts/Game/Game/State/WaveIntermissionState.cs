using Assets.Scripts.BlackHole;
using UnityEngine;

public class WaveIntermissionState : GameBaseState
{

    private float _intermissionTime;
    private IGameState _returnState;

    public WaveIntermissionState(Game game, float intermissionTime) : base(game)
    {
        _intermissionTime = intermissionTime;

        if (_game.CurrentWaveIndex == 0)
        {

            UpgradePanelController.Instance.Close();
            _returnState = GetWarmupState();
        }
        else
        {
            _returnState = this;
            UpgradePanelController.Instance.Open();
            BlackHoleCreator.Instance.SetDisabled();
            UpgradesController.Instance.onUpgrade += OnUpgrade;
        }
    }

    public override string StateName => "Intermission";

    public override IGameState Update(float deltaTime)
    {
        return _returnState;
    }

    public void OnUpgrade()
    {
        UpgradePanelController.Instance.CloseWithDelay(0.5f);
        BlackHoleCreator.Instance.SetEnabled();
        _returnState = GetWarmupState();
        
        UpgradesController.Instance.onUpgrade -= OnUpgrade;
    }

    public IGameState GetWarmupState()
    {
        return new WaveWarmupState(_game, Game.Instance.GeneralConfiguration.DefaultWarmupTime, _game.Level.Waves[_game.CurrentWaveIndex]);
    }
}