public interface IGameState
{
    IGameState Update(float deltaTime);
    string StateName { get; }
}