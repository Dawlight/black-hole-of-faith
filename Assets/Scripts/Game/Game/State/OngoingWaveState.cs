using System;
using Assets.Scripts.Enemy;
using Assets.Scripts.Monument;
using UnityEngine;

public class OngoingWaveState : GameBaseState
{

    private EnemyWave _currentWave;

    private float _timeToEnemySpawn;
    private IGameState _nextState;
    private float _timeSinceLastEnemySpawn = 0;
    private int _enemyToSpawnIndex = 0;

    private float _timeSinceLastCitizenSpawn = 0f;

    private int _enemiesKilled = 0;

    public OngoingWaveState(Game game, EnemyWave enemyWave, float spawnRate) : base(game)
    {
        _currentWave = enemyWave;
        _timeToEnemySpawn = 1 / spawnRate;
        _nextState = this;

        game.onEnemyKilledGlobal += OnEnemyKilled;
        Monument.Instance.onMonumentDeath += OnMonumentDeath;
    }

    private void OnMonumentDeath()
    {
        _nextState = new GameOverState(_game);
        Monument.Instance.onMonumentDeath -= OnMonumentDeath;
    }

    private void OnEnemyKilled(Enemy enemy)
    {
        _enemiesKilled++;
    }

    public override string StateName => "Ongoing Wave";

    public override IGameState Update(float deltaTime)
    {
        if (_nextState == this)
        {
            if (_enemiesKilled >= _currentWave.Enemies.Count)
            {
                _game.onEnemyKilledGlobal -= OnEnemyKilled;
                _nextState = new WaveClearedState(_game);
            }
            else
            {
                UpdateEnemySpawn(deltaTime);
                UpdateCitizenSpawn(deltaTime);
            }
        }

        return _nextState;
    }

    private void UpdateEnemySpawn(float deltaTime)
    {
        _timeSinceLastEnemySpawn += deltaTime;

        if (_enemyToSpawnIndex + 1 > _currentWave.Enemies.Count) return;

        while (_timeSinceLastEnemySpawn > _timeToEnemySpawn)
        {
            _timeSinceLastEnemySpawn -= _timeToEnemySpawn;

            SpawnEnemy();
        }
    }

    private void UpdateCitizenSpawn(float deltaTime)
    {
        if (Game.Instance.CitizensAlive.Count >= Game.Instance.MaxCitizens) return;

        var timeToCitizenSpawn = 1 / Game.Instance.CitizenSpawnRate;

        while (_timeSinceLastCitizenSpawn > timeToCitizenSpawn)
        {
            _timeSinceLastCitizenSpawn -= timeToCitizenSpawn;
            Game.Instance.SpawnNewCitizen();
        }

        _timeSinceLastCitizenSpawn += deltaTime;
    }


    void SpawnEnemy()
    {
        Game.Instance.SpawnNewEnemy(_currentWave.Enemies[_enemyToSpawnIndex]);
        _enemyToSpawnIndex++;
    }
}