public class LevelClearedState : GameBaseState
{
    public LevelClearedState(Game game) : base(game)
    {
    }

    public override string StateName => "Level Cleared";

    public override IGameState Update(float deltaTime)
    {
        return this;
    }
}