using UnityEngine;

public class WaveClearedState : GameBaseState
{

    private IGameState _returnState;

    public WaveClearedState(Game game) : base(game)
    {
        PauseMenuController.Instance.WavesCleared.text = game.CurrentWaveIndex + " WAVES CLEARED";

        game.CurrentWaveIndex++;

        var blackHoles = game.BlackHolesActive.ToArray();
        foreach (var blackHole in blackHoles)
        {
            blackHole.Kill(Quaternion.identity, 0);
        }

        if (game.CurrentWaveIndex + 1 > game.Level.Waves.Count)
        {
            _returnState = new LevelClearedState(game);
        }
        else
        {
            _returnState = new WaveIntermissionState(game, Game.Instance.GeneralConfiguration.DefaultIntermissionTime);
        }

    }

    public override string StateName => "Wave Cleared";

    public override IGameState Update(float deltaTime)
    {
        return _returnState;
    }
}