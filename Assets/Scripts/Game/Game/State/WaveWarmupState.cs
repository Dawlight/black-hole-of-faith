using UnityEngine;

public class WaveWarmupState : GameBaseState
{

    private float _warmupTime;
    private float _elapsedTime = 0;
    private EnemyWave _nextWave;
    private float _timeSinceLastCitizenSpawn;

    public WaveWarmupState(Game game, float warmupTime, EnemyWave nextWave) : base(game)
    {
        _warmupTime = warmupTime;
        _nextWave = nextWave;
    }

    public override string StateName => "Warmup";

    public override IGameState Update(float deltaTime)
    {

        if (_elapsedTime > _warmupTime)
        {
            return new OngoingWaveState(_game, _nextWave, 1);
        }

        UpdateCitizenSpawn(Time.deltaTime);

        _elapsedTime += deltaTime;

        return this;
    }

    private void UpdateCitizenSpawn(float deltaTime)
    {
        if (Game.Instance.CitizensAlive.Count >= Game.Instance.MaxCitizens) return;

        var timeToCitizenSpawn = 1 / Game.Instance.CitizenSpawnRate;

        while (_timeSinceLastCitizenSpawn > timeToCitizenSpawn)
        {
            _timeSinceLastCitizenSpawn -= timeToCitizenSpawn;
            Game.Instance.SpawnNewCitizen();
        }

        _timeSinceLastCitizenSpawn += deltaTime;
    }


}