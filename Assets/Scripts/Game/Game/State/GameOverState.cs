using System.Collections;
using UnityEngine;

public class GameOverState : GameBaseState
{
    public GameOverState(Game game) : base(game)
    {
        var enemiesAlive = game.EnemiesAlive.ToArray();

        foreach (var enemy in enemiesAlive)
        {
            enemy.Kill();
        }

        game.StartCoroutine(PauseGame(1f));
    }

    public override string StateName => "Game Over";

    public override IGameState Update(float deltaTime)
    {
        return this;
    }

    private IEnumerator PauseGame(float delay)
    {
        yield return new WaitForSecondsRealtime(delay);
        PauseMenuController.Instance.Open();
    }
}