﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CitizenSpawnPoint : MonoBehaviour
{
    [SerializeField] private Citizen _citizenPrefab = null;
    public Citizen CitizenPrefab { get { return _citizenPrefab; } }

    private float _spawnTime;

    public void Start()
    {
        _spawnTime = 1f / Game.Instance.CitizenSpawnRate;
    }

    public void Update()
    {
        // while (_timeSinceLastSpawn > _spawnTime)
        // {
        //     _timeSinceLastSpawn -= _spawnTime;

        //     TrySpawnCitizen();
        // }

        // _timeSinceLastSpawn += Time.deltaTime;
    }

    private void TrySpawnCitizen()
    {
        if (Game.Instance.CitizensAlive.Count < Game.Instance.MaxCitizens)
        {
            Game.Instance.SpawnNewCitizen();
        }
    }
}