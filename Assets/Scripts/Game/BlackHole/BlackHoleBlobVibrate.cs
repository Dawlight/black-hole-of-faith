﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHoleBlobVibrate : MonoBehaviour
{
    private Vector3 _originalPosition;
    private Coroutine _shakeBlobEnumerator;
    

    public void ShakeBlob(float frequency, float magnitude)
    {
        _originalPosition = transform.localPosition;
        _shakeBlobEnumerator = StartCoroutine(BlobShakeCoroutine(frequency, magnitude));
    }

    public void StopShakeBlob()
    {
        StopCoroutine(_shakeBlobEnumerator);
        transform.localPosition = _originalPosition;
    }

    IEnumerator BlobShakeCoroutine(float frequency, float magnitude)
    {
        var timeSinceLastShake = 0f;
        var timeBetweenShakes = 1f / frequency;
        var currentVelocity = new Vector3();

        while (true)
        {
            while (timeSinceLastShake > timeBetweenShakes)
            {
                timeSinceLastShake -= timeBetweenShakes;

                var x = Random.Range(-1f, 1f) * magnitude;
                var y = Random.Range(-1f, 1f) * magnitude;
                transform.localPosition = Vector3.SmoothDamp(transform.localPosition, _originalPosition + new Vector3(x, y), ref currentVelocity, 0.01f, 100f);
            }

            timeSinceLastShake += Time.deltaTime;

            yield return null;
        }
    }
}
