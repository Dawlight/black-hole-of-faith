﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.BlackHole
{
    public class BlackHoleCore : MonoBehaviour
    {
        public delegate void OnCoreTouch(Rigidbody2D rigidBody2D);
        public delegate void OnCoreDetouch(Rigidbody2D rigidBody2D);

        public OnCoreTouch onCoreTouch;
        public OnCoreDetouch onCoreDetouch;

        private List<Rigidbody2D> _rigidBody2D = new List<Rigidbody2D>();
        private CircleCollider2D _circleCollider2D;

        void Start()
        {
            _circleCollider2D = GetComponent<CircleCollider2D>();
        }

        void OnTriggerEnter2D(Collider2D collider2D)
        {
            if (collider2D.gameObject.tag == "Citizen")
            {
                var citizenRigidBody2D = collider2D.gameObject.GetComponent<Rigidbody2D>();

                if (onCoreTouch == null) return;

                onCoreTouch(citizenRigidBody2D);
            }
        }

        public void SetTriggerEnabled(bool enabled) => GetComponent<Collider2D>().enabled = enabled;
    }
}