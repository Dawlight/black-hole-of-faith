﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;

public class BlackHoleBlobifier : MonoBehaviour
{
    public delegate void OnCitizenEnterBlobArea(CitizenBlobController blobController);
    public delegate void OnCitizenExitBlobArea(CitizenBlobController blobController);

    public OnCitizenEnterBlobArea onCitizenEnterBlobArea;
    public OnCitizenExitBlobArea onCitizenExitBlobArea;

    // Start is called before the first frame update
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (enabled == false) return;

       
        if (collider.gameObject.tag == Tags.Citizen)
        {
            var blobController = collider.gameObject.GetComponent<CitizenBlobController>();
            blobController.SetBlobSize(0);
            blobController.SetTarget(transform.root);
            onCitizenEnterBlobArea?.Invoke(blobController);
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (enabled == false) return;

        if (collider.gameObject.tag == Tags.Citizen)
        {
            var blobController = collider.gameObject.GetComponent<CitizenBlobController>();
            blobController.SetTarget(null);
            blobController.SetBlobSize(0);
            onCitizenExitBlobArea?.Invoke(blobController);
        }
    }
}
