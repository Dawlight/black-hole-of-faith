﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.BlackHole.BlackHoleStates
{
    public class FollowState : BaseState
    {

        private IBlackHoleState _returnState;

        public FollowState(BlackHole blackHole) : base(blackHole)
        {
            _returnState = this;


            BlackHole.StartingRadius = 0.5f;
            BlackHole.UpdateSize();

            BlackHole.Children.VacuumTrigger.SetVortexEnabled(true);
            BlackHole.SetStickyCoreEnabled(true);


            // BlackHole.ParticleSystems.SuckyParticles.Emit(100);

            BlackHole.ParticleSystems.BlackHoleOpen.Play();
            BlackHole.ParticleSystems.SuckyParticles.Play();
            var emission = BlackHole.ParticleSystems.SuckyParticles.emission;
            emission.rateOverTime = 60;

            var velocityOverLifetime = BlackHole.ParticleSystems.SuckyParticles.velocityOverLifetime;
            velocityOverLifetime.orbitalZ = 0;

            CameraShaker.Instance.ShakeCamera(0.1f, 100, 0.5f);
            // var externalForces = BlackHole.SuckyParticles.externalForces;
            // externalForces.multiplier = 1.5f;

            BlackHole.PlaySound(AudioBank.Instance.BlackHoleEffects.FullyFormed, 1f);

            BlackHole.ParticleSystems.DrippyParticles.Stop();

            BlackHole.OriginalCreator.onBlackHoleRelease += OnBlackHoleRelease;

            Game.Instance.EnemiesAlive.ForEach(enemy =>
            {
                enemy.EnemyLook.LookTarget = blackHole.transform.root;
            });

            BlackHole.Children.BlobVibrate.ShakeBlob(50, 0.1f);
        }

        public override string StateName => typeof(FollowState).ToString();

        public override IBlackHoleState HandleInput()
        {
            return _returnState;
        }

        public override void OnKilled()
        {
            BlackHole.OriginalCreator.onBlackHoleRelease -= OnBlackHoleRelease;
        }

        public override IBlackHoleState Update(float deltaTime)
        {
            return _returnState;
        }

        void OnBlackHoleRelease()
        {
            if (BlackHole.CitizenCount > 0)
            {
                _returnState = new RestingState(BlackHole, 0.25f);
            }
            else
            {
                _returnState = new FadeState(BlackHole, 0.5f);
            }

            BlackHole.OriginalCreator.onBlackHoleRelease -= OnBlackHoleRelease;
        }
    }
}
