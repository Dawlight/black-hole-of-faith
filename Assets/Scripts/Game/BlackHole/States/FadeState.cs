using Assets.Scripts.BlackHole;
using Assets.Scripts.BlackHole.BlackHoleStates;
using UnityEngine;

public class FadeState : BaseState
{
    private readonly float _fadeTime;
    private float _elapsedTime;
    private float _startingRadiusCache;

    public FadeState(BlackHole blackHole, float fadeTime) : base(blackHole)
    {
        _fadeTime = fadeTime;
        _startingRadiusCache = BlackHole.StartingRadius;

        BlackHole.Children.VacuumTrigger.SetVortexEnabled(false);
        BlackHole.SetStickyCoreEnabled(false);
        BlackHole.SetGravityEnabled(false);
        BlackHole.StopFollowing();

        BlackHole.Children.Blobifier.enabled = false;

        var emission = BlackHole.ParticleSystems.SuckyParticles.emission;
        emission.rateOverTime = 0;


        BlackHole.PlaySound(AudioBank.Instance.BlackHoleEffects.Fading, 0.5f);
    }

    public override string StateName => typeof(FadeState).ToString();

    public override IBlackHoleState HandleInput()
    {
        return this;
    }

    public override void OnKilled()
    {
    }

    public override IBlackHoleState Update(float deltaTime)
    {

        if (_elapsedTime > _fadeTime)
        {
            BlackHole.KillSilent();
        }

        _elapsedTime += deltaTime;

        BlackHole.StartingRadius = (1 - (_elapsedTime / _fadeTime)) * _startingRadiusCache;
        BlackHole.UpdateSize();

        return this;
    }
}