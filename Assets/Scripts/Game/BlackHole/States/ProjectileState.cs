﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.BlackHole.BlackHoleStates
{
    public class ProjectileState : BaseState
    {
        public ProjectileState(BlackHole blackHole) : base(blackHole)
        {
            BlackHole.Children.VisualCore.color = Color.blue;

            BlackHole.Children.VacuumTrigger.SetVortexEnabled(false);
            BlackHole.SetStickyCoreEnabled(false);
            BlackHole.SetGravityEnabled(true);
            BlackHole.SetCollisionEnabled(true);

            BlackHole.ParticleSystems.SuckyParticles.Stop();
            BlackHole.ParticleSystems.DrippyParticles.Play();
            BlackHole.ParticleSystems.TrailParticles.Play();

            BlackHole.Children.Blobifier.enabled = false;

            BlackHole.PlaySound(AudioBank.Instance.BlackHoleEffects.Fling, 1);

            CameraShaker.Instance.ShakeCamera(0.1f, 100, 0.2f);
        }

        public override string StateName => typeof(ProjectileState).ToString();

        public override IBlackHoleState HandleInput()
        {
            return this;
        }

        public override void OnKilled()
        {
        }

        public override IBlackHoleState Update(float deltaTime)
        {
            return this;
        }
    }
}
