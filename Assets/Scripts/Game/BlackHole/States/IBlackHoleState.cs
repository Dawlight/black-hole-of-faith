﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.BlackHole.BlackHoleStates
{
    public interface IBlackHoleState
    {
        IBlackHoleState HandleInput();
        IBlackHoleState Update(float deltaTime);
        void OnKilled();
    }
}
