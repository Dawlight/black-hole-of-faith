using Assets.Scripts.BlackHole;
using Assets.Scripts.BlackHole.BlackHoleStates;
using UnityEngine;

public class FormingState : BaseState
{
    private readonly float _formationTime;
    private float _elapsedTime;
    private IBlackHoleState _returnState;
    private float _startingRadiusCache;

    public FormingState(BlackHole blackHole, float formationTime) : base(blackHole)
    {
        _formationTime = formationTime;
        _returnState = this;
        _startingRadiusCache = BlackHole.StartingRadius;

        BlackHole.Children.VisualCore.color = Color.magenta;

        BlackHole.StartingRadius = 0;
        BlackHole.UpdateSize();

        BlackHole.Children.VacuumTrigger.SetVortexEnabled(false);
        BlackHole.SetStickyCoreEnabled(false);
        BlackHole.SetGravityEnabled(false);
        BlackHole.StartFollowing(blackHole.OriginalCreator.transform.root);
        BlackHole.SetCollisionEnabled(false);

        BlackHole.ParticleSystems.SuckyParticles.Play();
        var emission = BlackHole.ParticleSystems.SuckyParticles.emission;
        emission.rateOverTime = 30;

        BlackHole.PlaySound(AudioBank.Instance.BlackHoleEffects.Forming, 0.5f);

        BlackHole.OriginalCreator.onBlackHoleRelease += OnBlackHoleRelease;
    }

    public override string StateName => typeof(FormingState).ToString();

    public override IBlackHoleState HandleInput()
    {
        return _returnState;
    }

    public override void OnKilled()
    {
        BlackHole.OriginalCreator.onBlackHoleRelease -= OnBlackHoleRelease;
    }

    public override IBlackHoleState Update(float deltaTime)
    {
        if (_elapsedTime > _formationTime)
        {
            _elapsedTime = _formationTime;
            BlackHole.OriginalCreator.onBlackHoleRelease -= OnBlackHoleRelease;

            return new FollowState(BlackHole);
        }
        else
        {
            _elapsedTime += deltaTime;
        }

        return _returnState;
    }

    void OnBlackHoleRelease()
    {
        _returnState = new FadeState(BlackHole, 0.5f);
        BlackHole.OriginalCreator.onBlackHoleRelease -= OnBlackHoleRelease;
    }
}