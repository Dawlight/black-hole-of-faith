﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.BlackHole.BlackHoleStates
{
    public abstract class BaseState : IBlackHoleState
    {
        private BlackHole _blackHole;
        public BlackHole BlackHole { get { return _blackHole; } }

        public BaseState(BlackHole blackHole)
        {
            _blackHole = blackHole;
        }

        public abstract void OnKilled();
        abstract public IBlackHoleState HandleInput();
        abstract public IBlackHoleState Update(float deltaTime);
        public abstract string StateName { get; }
    }
}
