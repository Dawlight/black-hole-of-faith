﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.BlackHole.BlackHoleStates
{
    public class RestingState : BaseState
    {

        private IBlackHoleState _returnState;
        private float _suckyParticlesLingerTime;
        private float _timeElapsed = 0;

        public RestingState(BlackHole blackHole, float suckyParticlesLingerTime = 0) : base(blackHole)
        {
            _returnState = this;
            _suckyParticlesLingerTime = suckyParticlesLingerTime;

            BlackHole.Children.VisualCore.color = Color.green;

            BlackHole.StopFollowing();
            BlackHole.Children.VacuumTrigger.SetVortexEnabled(false);
            BlackHole.SetGravityEnabled(false);
            BlackHole.SetStickyCoreEnabled(true);
            BlackHole.BlackHoleFollow.FollowTransform = Game.Instance.GameObjectConfiguration.BlackHoleRestingTransform;

            var externalForces = BlackHole.ParticleSystems.SuckyParticles.externalForces;
            externalForces.multiplier = 0;

            var velocityOverTime = BlackHole.ParticleSystems.SuckyParticles.velocityOverLifetime;
            velocityOverTime.radial = 1;

            BlackHole.Children.BlobVibrate.StopShakeBlob();

            BlackHole.onStartDragging += OnBlackHoleStartDragging;
        }

        public override string StateName => typeof(RestingState).ToString();

        public override IBlackHoleState HandleInput()
        {
            return _returnState;
        }

        public override IBlackHoleState Update(float deltaTime)
        {
            var velocity = BlackHole.HoleRigidBody.velocity;
            velocity.x = Mathf.Min(velocity.x, 20);
            velocity.y = Mathf.Min(velocity.y, 20);

            BlackHole.HoleRigidBody.velocity = velocity;

            DoSuckyParticlesLinger();

            _timeElapsed += deltaTime;

            return _returnState;
        }

        public void DoSuckyParticlesLinger()
        {

            if (_timeElapsed > _suckyParticlesLingerTime)
            {
                BlackHole.ParticleSystems.SuckyParticles.Stop();
            }
        }

        public void OnBlackHoleStartDragging(Transform draggingTransform, Vector3 dragStartPosition)
        {
            _returnState = new DraggingState(BlackHole, draggingTransform, dragStartPosition);
            BlackHole.onStartDragging -= OnBlackHoleStartDragging;
        }

        public override void OnKilled()
        {
            BlackHole.onStartDragging -= OnBlackHoleStartDragging;
        }
    }
}
