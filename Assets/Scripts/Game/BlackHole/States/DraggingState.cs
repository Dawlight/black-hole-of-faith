using Assets.Scripts.BlackHole;
using Assets.Scripts.BlackHole.BlackHoleStates;
using UnityEngine;
using static Assets.Scripts.BlackHole.BlackHole;

public class DraggingState : BaseState
{
    private Transform _draggingTransform;
    private Vector3 _dragStartPosition;
    private IBlackHoleState _returnState;
    private GameObject _followObject;

    public DraggingState(BlackHole blackHole, Transform draggingTransform, Vector3 dragStartPosition) : base(blackHole)
    {
        _draggingTransform = draggingTransform;
        _dragStartPosition = dragStartPosition;
        _returnState = this;

        _followObject = new GameObject();
        _draggingTransform.position = dragStartPosition;
        BlackHole.BlackHoleFollow.FollowTransform = _followObject.transform;

        BlackHole.HoleRigidBody.drag = 2f;

        BlackHole.onSling += OnSling;
    }

    public override string StateName => typeof(DraggingState).ToString();

    public override IBlackHoleState HandleInput()
    {
        return _returnState;
    }

    public override IBlackHoleState Update(float deltaTime)
    {

        var draggingVector = _dragStartPosition - _draggingTransform.position;
        var reducedDragginVector = draggingVector / ( 1 + (draggingVector.magnitude / 10f));

        _followObject.transform.position = _dragStartPosition - reducedDragginVector;

        return _returnState;
    }

    public void OnSling(Vector3 slingVector)
    {
        BlackHole.BlackHoleFollow.FollowTransform = null;
        BlackHole.DestroyImmediate(_followObject);
        _returnState = new ProjectileState(BlackHole);
        BlackHole.onSling -= OnSling;
    }

    public override void OnKilled()
    {
         BlackHole.onSling -= OnSling;
    }
}