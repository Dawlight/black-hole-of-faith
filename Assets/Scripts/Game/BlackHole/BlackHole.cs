﻿using Assets.Scripts.BlackHole.BlackHoleStates;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.BlackHole
{
    public class BlackHole : MonoBehaviour
    {
        public delegate void OnKilled(BlackHole blackHole);
        public delegate void OnSling(Vector3 slingVector);
        public delegate void OnStartDragging(Transform draggingTransform, Vector3 draggingStartPosition);

        public event OnKilled onKilled;
        public event OnSling onSling;
        public event OnStartDragging onStartDragging;

        public float DamagePerMass;
        public float CitizenArea;
        public float AreaWeight;

        public float Damage { get { return DamagePerMass * HoleRigidBody.mass; } }

        public float StartingRadius;
        public BlackHoleExplosionController ExplosionPrefab;

        public int CitizenCount { get; private set; } = 0;

        public AudioSource AudioSource { get { return _audioSource; } }
        public BlackHoleFollow BlackHoleFollow { get { return _blackHoleFollow; } }
        public BlackHoleCreator OriginalCreator { get { return _originalCreator; } }
        public Rigidbody2D HoleRigidBody { get { return _holeRigidBody; } }
        public CircleCollider2D CircleCollider { get { return _circleCollider; } }
        public IBlackHoleState State { get { return _state; } }

        [SerializeField] public ParticleSystems ParticleSystems;
        [SerializeField] public ChildReferences Children;


        private AudioSource _audioSource;
        private BlackHoleFollow _blackHoleFollow;
        public BlackHoleCreator _originalCreator;
        private Rigidbody2D _holeRigidBody;
        private CircleCollider2D _circleCollider;
        private List<Rigidbody2D> _citizenRigidBodies = new List<Rigidbody2D>();
        private IBlackHoleState _state;

        private float _radius;

        private float _startingRotationSpeed = 360;
        private float _rotationSpeed;
        private float _speedDecreasePerCitizen = 5;

        private void Awake()
        {
            _blackHoleFollow = GetComponent<BlackHoleFollow>();
            _holeRigidBody = GetComponent<Rigidbody2D>();
            _circleCollider = GetComponent<CircleCollider2D>();
            _audioSource = GetComponent<AudioSource>();
        }

        void Start()
        {
            _state = new FormingState(this, 0.5f);

            Children.VacuumTrigger.onVacuumEnter += OnVacuumEnter;
            Children.VacuumTrigger.onVacuumExit += OnVacuumExit;
            Children.StickyCore.onCoreTouch += OnCoreTouch;

            _rotationSpeed = _startingRotationSpeed;

            UpdateSize();
        }


        private void Update()
        {
            _state = _state?.HandleInput();
            _state = _state?.Update(Time.deltaTime);

            RotateCollector();
        }



        public void PlaySound(AudioClip clip, float pitch)
        {
            _audioSource.clip = clip;
            _audioSource.pitch = pitch;
            _audioSource.Play();
        }

        public void SetCreator(BlackHoleCreator creator)
        {
            _originalCreator = creator;
        }

        public void Sling(Vector3 slingVector)
        {
            onSling?.Invoke(slingVector);

            HoleRigidBody.AddForce(slingVector * Game.Instance.SlingMagnitude, ForceMode2D.Force);
        }

        public void StartDragging(Transform draggingTransform, Vector3 draggingStartPosition)
        {
            onStartDragging?.Invoke(draggingTransform, draggingStartPosition);
        }

        public void Kill(Quaternion rotation, float explosionMagnitude)
        {
            onKilled?.Invoke(this);
            _state.OnKilled();

            var explosionController = Instantiate(ExplosionPrefab, transform.position, rotation);
            explosionController.SetParticleEmitterShapeRadius(_radius);
            explosionController.SetParticleStartingSpeedScale(explosionMagnitude);
            explosionController.SetHumanParticleAmount(CitizenCount * 4);

            Game.Instance.Freeze(0.05f);
            CameraShaker.Instance.ShakeCamera(0.4f, 40, 0.25f);

            Game.Instance.PlaySound(AudioBank.Instance.BlackHoleEffects.Explode, 1);
            Game.Instance.StartCoroutine(DetachAndKillParticles(2f));
            ParticleSystems.TrailParticles.Stop();
            ParticleSystems.DrippyParticles.Stop();

            var shape = ParticleSystems.TrailExplosionParticles.shape;
            shape.radius = _radius;
            ParticleSystems.TrailExplosionParticles.Play();
            Destroy(gameObject);
        }

        IEnumerator DetachAndKillParticles(float delay)
        {
            Children.ParticlesObject.transform.parent = null;
            var timeElapsed = 0f;


            while (timeElapsed < delay)
            {
                timeElapsed += Time.deltaTime;
                yield return 0;
            }

            Destroy(Children.ParticlesObject);
        }

        public void KillSilent()
        {
             _state.OnKilled();
            // TODO:
            // Is this really silent though?
            onKilled?.Invoke(this);
            
            Destroy(gameObject);
        }

        void OnVacuumEnter(Rigidbody2D rigidbody2D)
        {
            _citizenRigidBodies.Add(rigidbody2D);
            rigidbody2D.AddTorque(UnityEngine.Random.Range(-20f, 20f));
        }

        void OnVacuumExit(Rigidbody2D rigidbody2D)
        {
            _citizenRigidBodies.Remove(rigidbody2D);
        }

        void OnCoreTouch(Rigidbody2D rigidbody2D)
        {
            CitizenCount++;
            UpdateSize();

            _citizenRigidBodies.Remove(rigidbody2D);

            var citizen = rigidbody2D.GetComponent<Citizen>();
            citizen.KillSilent(Children.BlobCollector);

            var pitch = 0.5f + 0.05f * CitizenCount;
            PlaySound(AudioBank.Instance.BlackHoleEffects.Blobbify, pitch);
        }

        public void OnCollisionEnter2D(Collision2D collision)
        {
            var rigidBody = GetComponent<Rigidbody2D>();
            var normal = (Vector3)collision.contacts[0].normal;
            normal = new Vector2(normal.x * -1, normal.y * -1);
            var velocity = (Vector3)rigidBody.velocity.normalized;

            var rotation = Quaternion.LookRotation(Vector3.forward, velocity.normalized);

            Kill(rotation, collision.relativeVelocity.magnitude / 4);
        }



        private void RotateCollector()
        {
            var rotationInDegrees = Time.deltaTime * _rotationSpeed;

            var rotation = Children.BlobCollector.rotation;
            var euler = rotation.eulerAngles;
            rotation = Quaternion.Euler(0, 0, euler.z + rotationInDegrees);

            Children.BlobCollector.rotation = rotation;
        }


        public void UpdateSize()
        {
            _rotationSpeed = _startingRotationSpeed - _speedDecreasePerCitizen * CitizenCount;

            var totalArea = CitizenCount * CitizenArea;
            _radius = CalculateRadiusFromArea(totalArea) + StartingRadius;

            _holeRigidBody.mass = totalArea * AreaWeight;

            Children.Blob.gameObject.transform.localScale = new Vector3(_radius, _radius);
            Children.Blob.gameObject.transform.localScale = new Vector3(_radius * 2, _radius * 2);
            Children.Blob.gameObject.transform.localEulerAngles = new Vector3(_radius * 2, _radius * 2);
            _circleCollider.radius = _radius * 0.5f;
            Children.StickyCore.transform.localScale = new Vector3(_radius / 2f, _radius / 2f);
        }

        private float CalculateRadiusFromArea(float area) => Mathf.Sqrt(area / Mathf.PI);

        public void StartFollowing(Transform followTransform) => BlackHoleFollow.FollowTransform = followTransform;

        public void StopFollowing() => BlackHoleFollow.FollowTransform = null;

        public void SetGravityEnabled(bool enabled = true)
        {
            HoleRigidBody.gravityScale = enabled ? 1f : 0f;
            HoleRigidBody.drag = enabled ? 0 : 10;
        }

        public void SetCollisionEnabled(bool enabled) => CircleCollider.isTrigger = !enabled;

        public void SetStickyCoreEnabled(bool enabled)
        {
            Children.StickyCore.SetTriggerEnabled(enabled);
        }

        void OnDisable()
        {
            Children.VacuumTrigger.onVacuumEnter -= OnVacuumEnter;
            Children.VacuumTrigger.onVacuumExit -= OnVacuumExit;
            Children.StickyCore.onCoreTouch -= OnCoreTouch;
            onKilled = null;
            onSling = null;
            onStartDragging = null;
        }
    }

    [Serializable]
    public struct ParticleSystems
    {
        public ParticleSystem BlackHoleOpen;
        public ParticleSystem SuckyParticles;
        public ParticleSystem DrippyParticles;
        public ParticleSystem TrailParticles;
        public ParticleSystem TrailExplosionParticles;
    }

    [Serializable]
    public struct ChildReferences
    {
        [SerializeField] private Transform _blobCollector;
        [SerializeField] private BlackHoleBlobifier _blobifier;
        [SerializeField] private GameObject _blob;
        [SerializeField] private BlackHoleVortex _vacuumTrigger;
        [SerializeField] private BlackHoleCore _stickyCore;
        [SerializeField] private SpriteRenderer _visualCore;
        [SerializeField] private SpriteRenderer _blobRenderer;
        [SerializeField] private BlackHoleBlobVibrate _blobVibrate;
        [SerializeField] private GameObject _particlesObject;

        public Transform BlobCollector { get { return _blobCollector; } }
        public BlackHoleBlobifier Blobifier { get { return _blobifier; } }
        public GameObject Blob { get { return _blob; } }
        public BlackHoleVortex VacuumTrigger { get { return _vacuumTrigger; } }
        public BlackHoleCore StickyCore { get { return _stickyCore; } }
        public SpriteRenderer VisualCore { get { return _visualCore; } }
        public SpriteRenderer BlobRenderer { get { return _blobRenderer; } }
        public BlackHoleBlobVibrate BlobVibrate { get { return _blobVibrate; } }
        public GameObject ParticlesObject { get { return _particlesObject; } }
    }
}