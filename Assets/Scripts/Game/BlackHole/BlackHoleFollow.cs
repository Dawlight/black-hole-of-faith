﻿using UnityEngine;

namespace Assets.Scripts.BlackHole
{
    public class BlackHoleFollow : MonoBehaviour
    {
        [SerializeField] [Range(0, 10000)] private float _followForce;
        public Transform FollowTransform { get; set; }

        public float SmoothTime = 0.1f;
        public float MaxSpeed = 100f;

        private Vector3 _currentVelocity = new Vector3();

        private Rigidbody2D _rigidBody2D;

        void Start()
        {
            _rigidBody2D = GetComponent<Rigidbody2D>();
        }

        void FixedUpdate()
        {
            if (FollowTransform != null)
            {
                transform.position = Vector3.SmoothDamp(transform.position, FollowTransform.position, ref _currentVelocity, 0.1f, 100f);
            }
        }
    }
}