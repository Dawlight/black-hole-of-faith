﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.BlackHole
{
    public class BlackHoleVortex : MonoBehaviour
    {
        public delegate void OnVacuumEnter(Rigidbody2D rigidBody2D);
        public delegate void OnVacuumExit(Rigidbody2D rigidBody2D);

        public OnVacuumEnter onVacuumEnter;
        public OnVacuumExit onVacuumExit;

        private PointEffector2D _pointAffector;
        public PointEffector2D PointEffector { get { return _pointAffector; } }

        public void Awake()
        {
            _pointAffector = GetComponent<PointEffector2D>();
        }

        public void SetTriggerEnabled(bool enabled)
        {
            GetComponent<Collider2D>().enabled = enabled;
            GetComponent<PointEffector2D>().enabled = enabled;
        }

        void OnTriggerEnter2D(Collider2D collider2D)
        {
            if (collider2D.gameObject.tag == "Citizen")
            {
                var rigidBody2D = collider2D.gameObject.GetComponent<Rigidbody2D>();

                if (onVacuumEnter == null) return;
                onVacuumEnter(rigidBody2D);
            }
        }

        void OnTriggerExit2D(Collider2D collider2D)
        {
            if (collider2D.gameObject.tag == "Citizen")
            {
                var rigidBody2D = collider2D.gameObject.GetComponent<Rigidbody2D>();

                if (onVacuumExit == null) return;
                onVacuumExit(rigidBody2D);
            }
        }

          public void SetVortexEnabled(bool enabled)
        {
            this.enabled = enabled;
            PointEffector.enabled = enabled;
            SetTriggerEnabled(enabled);
        }
    }
}