﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Citizen : MonoBehaviour
{

    public GameObject Blob;
    public delegate void OnDeath(Citizen citizen);
    public OnDeath onKilled;

    public bool Stuck = false;
    private float _horizontalVelocity = 2f;
    private Rigidbody2D _rigidBody;
    private SpriteRenderer _sprintRenderer;
    private Animator _animator;

    private float _timeSinceBlobActivated;
    private float _blobActivationTime;


    void Awake()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
        _sprintRenderer = GetComponent<SpriteRenderer>();
        _horizontalVelocity = Random.Range(1.5f, 4.5f);
        _animator = GetComponent<Animator>();
    }

    void Start()
    {
        var startDirection = Random.Range(0, 2);
        if (startDirection > 0)
        {
            _horizontalVelocity *= -1;
            _sprintRenderer.flipX = !_sprintRenderer.flipX;
        }

        _rigidBody.mass = Random.Range(0.5f, 1f);
    }

    void Update()
    {
        if (_rigidBody.velocity.y > 2)
        {
            _animator.SetBool("InAir", true);
            enabled = false;
            _rigidBody.freezeRotation = false;
        }

        _rigidBody.velocity = new Vector2(_horizontalVelocity, _rigidBody.velocity.y);
    }

    public void KillSilent(Transform newBlobParent = null)
    {
        onKilled?.Invoke(this);
        Blob.transform.parent = newBlobParent;

        Destroy(gameObject);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.relativeVelocity.magnitude > 5f)
        {
            KillSilent(Blob.transform.parent);
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "VillageBorder")
        {
            _horizontalVelocity *= -1;
            _sprintRenderer.flipX = !_sprintRenderer.flipX;
        }
    }
}
