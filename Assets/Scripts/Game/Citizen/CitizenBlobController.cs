﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CitizenBlobController : MonoBehaviour
{
    public GameObject _blobRenderer;

    private Transform _target;
    private float _cutoffDistance;

    public void Start()
    {
        SetBlobSize(0);
    }

    public void Update()
    {
        if (_target != null)
        {
            var test = transform.position - _target.transform.root.position;
            var currentDistanceToTarget = Vector3.Distance(transform.position, _target.transform.root.position);


            var scalingFactor = Mathf.Clamp(1 - (currentDistanceToTarget / _cutoffDistance), 0, 1);

            SetBlobSize(scalingFactor);
        } else {
            SetBlobSize(0);
        }

    }

    public void SetBlobSize(float size)
    {
        _blobRenderer.transform.localScale = new Vector3(size, size, size);
    }

    public void SetTarget(Transform target)
    {

        if (target != null)
            _cutoffDistance = Vector3.Distance(target.position, transform.position);
        _target = target;
    }
}
