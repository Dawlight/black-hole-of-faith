﻿using System;
using Assets.Scripts.Enemy;
using UnityEngine;

namespace Assets.Scripts.Monument
{
    public class Monument : Singleton<Monument>
    {
        public delegate void OnMonumentDeath();
        public OnMonumentDeath onMonumentDeath;

        [SerializeField] private MonumentHitCollider _hitCollider = null;
        public MonumentHitCollider HitCollider { get { return _hitCollider; } }

        public CircleCollider2D EnemyTarget;

        public MonumentHealthBar MonumentHealthBar;

        public float StartingHealth;
        [HideInInspector] public float Health;


        void Start()
        {
            Health = StartingHealth;
            _hitCollider.onMonumentHit += OnMonumentHit;
        }

        void Update()
        {

        }

        private void OnMonumentHit(EnemyProjectile projectile)
        {

            if (projectile != null)
            {
                SetHealth(Health - projectile.ProjectileSpecification.Damage);

                projectile.enabled = false;
                projectile.DestroyWithDelay(1f);

                if (Health <= 0)
                    KillMonument();

            }
        }

        public void SetHealth(float health)
        {
            if (health > StartingHealth)
                Health = StartingHealth;
            else
                Health = health;

            var normalizedHealth = Health / StartingHealth;
            MonumentHealthBar.SetCurrentHealthFactor(normalizedHealth);
        }

        private void KillMonument()
        {
            onMonumentDeath?.Invoke();
        }
    }
}