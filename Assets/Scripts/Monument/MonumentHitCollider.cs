﻿using Assets.Scripts.Enemy;
using UnityEngine;

namespace Assets.Scripts.Monument
{
    public class MonumentHitCollider : MonoBehaviour
    {
        public delegate void OnMonumentHit(EnemyProjectile projectileSpecification);
        public OnMonumentHit onMonumentHit;

        public MonumentShaker MonumentShaker;

        void OnCollisionEnter2D(Collision2D collision2D)
        {
            if (collision2D.gameObject.tag == Tags.EnemyProjectile)
            {
                var projectile = collision2D.gameObject.GetComponent<EnemyProjectile>();

                MonumentShaker.ShakeMonument(0.1f, 100f, 0.2f);

                onMonumentHit?.Invoke(projectile);
            }
        }
    }
}