﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(CameraBoundaries))]
public class CameraBoundariesEditor : Editor
{
    public float HandleSize = 0.5f;
    public float SnapSize = 5f;
    public string UndoString = "Size changed!";

    private Vector3 _southWestCorner;
    private Vector3 _northWestCorner;
    private Vector3 _northEastCorner;
    private Vector3 _southEastCorner;

    void Awake()
    {
        Tools.current = Tool.None;
    }

    void OnSceneGUI()
    {

        CameraBoundaries t = target as CameraBoundaries;
        _southWestCorner = t.transform.position;
        _northWestCorner = t.transform.position + new Vector3(0, t.Height);
        _northEastCorner = t.transform.position + new Vector3(t.Width, t.Height);
        _southEastCorner = t.transform.position + new Vector3(t.Width, 0);

        Handles.DrawDottedLines(new Vector3[] {
        _southWestCorner,
        _northWestCorner,
        _northWestCorner,
        _northEastCorner,
        _northEastCorner,
        _southEastCorner,
        _southEastCorner,
        _southWestCorner
        }, 2f);

        NorthEastCornerHandle(t);
        SouthEastCornerHandle(t);
        SouthWestCornerHandle(t);
        NorthWestCornerHandle(t);
    }

    private void NorthEastCornerHandle(CameraBoundaries boundaries)
    {
        EditorGUI.BeginChangeCheck();

        var newNorthEastCornerPosition = Handles.FreeMoveHandle(_northEastCorner, Quaternion.identity, HandleSize, Vector3.one * SnapSize, Handles.DotHandleCap);
        var newWidth = boundaries.Width + newNorthEastCornerPosition.x - _northEastCorner.x;
        var newHeight = boundaries.Height + newNorthEastCornerPosition.y - _northEastCorner.y;
        if (EditorGUI.EndChangeCheck())
        {
            boundaries.Width = newWidth;
            boundaries.Height = newHeight;
			Undo.RecordObject(boundaries, UndoString);
        }
    }

    private void SouthEastCornerHandle(CameraBoundaries boundaries)
    {
        EditorGUI.BeginChangeCheck();
        var newSouthEastCornerPosition = Handles.FreeMoveHandle(_southEastCorner, Quaternion.identity, HandleSize, Vector3.one * SnapSize, Handles.DotHandleCap);

        var newWidth = boundaries.Width + (newSouthEastCornerPosition.x - _southEastCorner.x);
        var newHeight = boundaries.Height + (_southEastCorner.y - newSouthEastCornerPosition.y);

        if (EditorGUI.EndChangeCheck())
        {
            boundaries.Width = newWidth;
            boundaries.Height = newHeight;
            boundaries.transform.position = new Vector3(boundaries.transform.position.x, newSouthEastCornerPosition.y);
            Undo.RecordObject(boundaries, UndoString);
			Undo.RecordObject(boundaries.transform, UndoString);
        }
    }

    private void SouthWestCornerHandle(CameraBoundaries boundaries)
    {
        EditorGUI.BeginChangeCheck();

        var newSouthWestCornerPosition = Handles.FreeMoveHandle(_southWestCorner, Quaternion.identity, HandleSize, Vector3.one * SnapSize, Handles.DotHandleCap);

        var newWidth = boundaries.Width + (_southWestCorner.x - newSouthWestCornerPosition.x);
        var newHeight = boundaries.Height + (_southWestCorner.y - newSouthWestCornerPosition.y);

        if (EditorGUI.EndChangeCheck())
        {
            boundaries.Width = newWidth;
            boundaries.Height = newHeight;
            boundaries.transform.position = newSouthWestCornerPosition;
			Undo.RecordObject(boundaries, UndoString);
			Undo.RecordObject(boundaries.transform, UndoString);
        }
    }

    private void NorthWestCornerHandle(CameraBoundaries boundaries)
    {
        EditorGUI.BeginChangeCheck();
        var newNorthWestCornerPosition = Handles.FreeMoveHandle(_northWestCorner, Quaternion.identity, HandleSize, Vector3.one * SnapSize, Handles.DotHandleCap);

        var newWidth = boundaries.Width + (_northWestCorner.x - newNorthWestCornerPosition.x);
        var newHeight = boundaries.Height + (newNorthWestCornerPosition.y - _northWestCorner.y);


        if (EditorGUI.EndChangeCheck())
        {
            boundaries.Width = newWidth;
            boundaries.Height = newHeight;
            boundaries.transform.position = new Vector3(newNorthWestCornerPosition.x, boundaries.transform.position.y);
			Undo.RecordObject(boundaries, UndoString);
			Undo.RecordObject(boundaries.transform, UndoString);
        }
    }
}
