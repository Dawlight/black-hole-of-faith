﻿using Assets.Scripts.BlackHole;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BlackHoleCreator))]
public class BlackHoleCreatorEditor : Editor
{

    void OnSceneGUI()
    {
        var t = target as BlackHoleCreator;
        Handles.DrawWireDisc(t.transform.position, Vector3.back, t.ClickRadius);
    }
}