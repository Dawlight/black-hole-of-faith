﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackHoleOfFaith
{


    public class Input : MonoBehaviour
    {


        public void Update()
        {
            var escape = UnityEngine.Input.GetButtonDown("Pause");

            if (escape)
            {
                if (PauseMenuController.Instance.Canvas.enabled)
                {
                   
                    PauseMenuController.Instance.Close();
                }
                else
                {
                    PauseMenuController.Instance.Open();
                }

                
            }

            if (UnityEngine.Input.GetKeyUp(KeyCode.U))
            {
                if (UpgradePanelController.Instance.IsOpen)
                    UpgradePanelController.Instance.Close();
                else
                    UpgradePanelController.Instance.Open();
            }

        }
    }
}