﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CameraBoundaries : MonoBehaviour
{


    public float Width;
    public float Height;
	[SerializeField]
    public float Left
    {
        get
        {
            return transform.position.x;
        }
    }

    public float Right
    {
        get
        {
            return transform.position.x + Width;
        }
    }

    public float Top
    {
        get
        {
            return transform.position.y + Height;
        }
    }

    public float Bottom
    {
        get
        {
            return transform.position.y;
        }
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
