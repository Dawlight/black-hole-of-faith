using UnityEngine;

public class DefaultPanningState : CameraPannerBaseState
{
    private Vector3 _velocity = new Vector3();

    public DefaultPanningState(CameraPanner cameraPanner) : base(cameraPanner)
    {
    }

    public override ICameraPannerState Update(float deltaTime)
    {

         var mouse = Input.mousePosition;
        var mouseWorldPosition = _cameraPanner.Camera.ScreenToWorldPoint(mouse);
        var mouseWorldVector = mouseWorldPosition - _cameraPanner.transform.position;
        var position = _cameraPanner.transform.position;

        var cameraScaleFactor = _cameraPanner.Camera.orthographicSize / _cameraPanner.CameraZoomer.MinCameraSize;

        // if (mouseWorldVector.magnitude < _cameraPanner.BoundCamera.VerticalExtent)
        // {
        //     mouseWorldPosition = _cameraPanner.transform.position;
        // };

        var targetVector = new Vector3(mouseWorldPosition.x, mouseWorldPosition.y, _cameraPanner.transform.position.z);

        _cameraPanner.transform.position = Vector3.SmoothDamp(_cameraPanner.transform.position, targetVector, ref _velocity, _cameraPanner.SmoothTime, _cameraPanner.MaxSpeed * cameraScaleFactor);

        return this;
    }

    public override void LateUpdate(float deltaTime)
    {
       

    }
}