public abstract class CameraPannerBaseState : ICameraPannerState
{
    protected CameraPanner _cameraPanner;

    public CameraPannerBaseState(CameraPanner cameraPanner)
    {
        _cameraPanner = cameraPanner;
    }

    public abstract void LateUpdate(float deltaTime);

    public abstract ICameraPannerState Update(float deltaTime);
}