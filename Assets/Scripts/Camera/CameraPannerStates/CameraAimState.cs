using Assets.Scripts.BlackHole;
using UnityEngine;

public class CameraAimState : CameraPannerBaseState
{
    private Vector3 _velocity = new Vector3();
    private BlackHole _blackHole;

    public CameraAimState(CameraPanner cameraPanner, BlackHole blackHole) : base(cameraPanner)
    {
        _blackHole = blackHole;
    }

    public override ICameraPannerState Update(float deltaTime)
    {
        var targetVector = Game.Instance.GameObjectConfiguration.BlackHoleRestingTransform.transform.position;
        _cameraPanner.transform.position = Vector3.SmoothDamp(_cameraPanner.transform.position, new Vector3(targetVector.x, targetVector.y, _cameraPanner.transform.position.z), ref _velocity, 0.1f, 100f);

        return this;
    }

    public override void LateUpdate(float deltaTime)
    {

    }
}