public interface ICameraPannerState 
{
    ICameraPannerState Update(float deltaTime);
    void LateUpdate(float deltaTime);
}