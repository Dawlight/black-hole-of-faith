﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxScroll : MonoBehaviour
{

    private Camera _camera;

    void Awake()
    {
        _camera = Camera.main;
    }
  
    void Update()
    {
        var cameraPosition = _camera.transform.position;
        var depth = transform.position.z * ParallaxScrollSettings.Instance.ParallaxScale;
        var newPosition = new Vector3(cameraPosition.x * depth, cameraPosition.y * depth, transform.position.z);

        transform.position = newPosition; 
    }
}
