﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSizeMatcher : MonoBehaviour
{
    private Camera _camera;
    public Camera CameraToMatch;

    void Awake()
    {
        _camera = GetComponent<Camera>();
    }

    void LateUpdate()
    {
        _camera.orthographicSize = CameraToMatch.orthographicSize;
    }
}
