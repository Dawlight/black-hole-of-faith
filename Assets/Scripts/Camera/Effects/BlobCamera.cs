﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class BlobCamera : MonoBehaviour
{


    public static string _globalBlobTextureName = "_GlobalBlobTexture";
   
    [SerializeField]
    private Camera _camera;

    void Start()
    {
        _camera = GetComponent<Camera>();

        if (_camera.targetTexture != null)
        {
            RenderTexture throwaway = _camera.targetTexture;
            _camera.targetTexture = null;
            DestroyImmediate(throwaway);
        }

        _camera.targetTexture = new RenderTexture(_camera.pixelWidth, _camera.pixelHeight, 16);
        _camera.targetTexture.filterMode = FilterMode.Bilinear;

        Shader.SetGlobalTexture(_globalBlobTextureName, _camera.targetTexture);

    }
}
