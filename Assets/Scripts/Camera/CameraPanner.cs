﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CameraZoomer), typeof(CameraBounder))]
public class CameraPanner : Singleton<CameraPanner>
{
    [SerializeField] private float _speed;

    private Camera _camera;
    private CameraBounder _boundCamera;
    private CameraZoomer _cameraZoom;
    private Vector3 center = new Vector3((float)Screen.width / 2, (float)Screen.height / 2);

    public Camera Camera { get { return _camera; } }
    public CameraBounder BoundCamera { get { return _boundCamera; } }
    public CameraZoomer CameraZoomer { get { return _cameraZoom; } }

    [Range(0f, 10f)] public float SmoothTime;
    [Range(0f, 50f)] public float MaxSpeed;

    public ICameraPannerState State;



    void Start()
    {
        _camera = Camera.main;
        _boundCamera = GetComponent<CameraBounder>();
        _cameraZoom = GetComponent<CameraZoomer>();
        State = new DefaultPanningState(this);
    }

    void Update()
    {
        State = State.Update(Time.deltaTime);

    }

    void LateUpdate()
    {
        State.LateUpdate(Time.deltaTime);
    }
}
