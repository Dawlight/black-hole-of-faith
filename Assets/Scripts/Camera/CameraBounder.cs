﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBounder : MonoBehaviour
{

    public float Left { get { return transform.position.x - (_cameraSize * _aspectRatio); } }
    public float Right { get { return transform.position.x + (_cameraSize * _aspectRatio); } }
    public float Top { get { return transform.position.y + _cameraSize; } }
    public float Bottom { get { return transform.position.y - _cameraSize; } }
    public float HorizontalExtent { get { return _cameraSize * _aspectRatio; } }
    public float VerticalExtent { get { return _cameraSize; } }

    private float _aspectRatio;
    private float _cameraSize;

    private Camera _camera;

    [SerializeField] private CameraBoundaries _cameraBoundaries = null;

    void Start()
    {
        _camera = Camera.main;
    }

    void Update()
    {

    }

    void LateUpdate()
    {
        _cameraSize = _camera.orthographicSize;
        _aspectRatio = (float)Screen.width / (float)Screen.height;


        var minX = _cameraBoundaries.Left + HorizontalExtent;
        var maxX = _cameraBoundaries.Right - HorizontalExtent;
        var minY = _cameraBoundaries.Bottom + VerticalExtent;
        var maxY = _cameraBoundaries.Top - VerticalExtent;

        var p = transform.position;

        p.x = Mathf.Clamp(p.x, minX, maxX);
        p.y = Mathf.Clamp(p.y, minY, maxY);

        transform.position = p;
    }
}
