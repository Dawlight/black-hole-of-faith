﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CameraBounder))]
public class CameraZoomer : Singleton<CameraZoomer>
{

    public float ScrollSpeed;
    public float MinCameraSize;
    public float MaxCameraSize;
    private Camera _camera;
    private float _currentVelocity;

    public float TargetSize;

    void Awake()
    {
        _camera = Camera.main;

    }

    void Start()
    {
        TargetSize = Camera.main.orthographicSize;
    }

    void Update()
    {
        var scrollWheelDelta = Input.mouseScrollDelta;
        TargetSize = TargetSize + -1f * scrollWheelDelta.y * ScrollSpeed;

        if (TargetSize > MaxCameraSize) TargetSize = MaxCameraSize;
        if (TargetSize < MinCameraSize) TargetSize = MinCameraSize;

        _camera.orthographicSize = Mathf.SmoothDamp(_camera.orthographicSize, TargetSize, ref _currentVelocity, 0.1f, 100f);
    }
}
