﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShaker : Singleton<CameraShaker>
{

    private Camera _camera;

    void Awake()
    {
        _camera = GetComponent<Camera>();
    }

    public void ShakeCamera(float duration, float frequency, float magnitude)
    {
        StartCoroutine(CameraShakeCoroutine(duration, frequency, magnitude));
    }

    IEnumerator CameraShakeCoroutine(float duration, float frequency, float magnitude)
    {
        var originalPosition = _camera.transform.localPosition;

        var timeElapsed = 0f;
        var timeSinceLastShake = 0f;
        var timeBetweenShakes = 1f / frequency;

        while (timeElapsed < duration)
        {
            while (timeSinceLastShake > timeBetweenShakes)
            {
                timeSinceLastShake -= timeBetweenShakes;

                var x = Random.Range(-1f, 1f) * magnitude;
                var y = Random.Range(-1f, 1f) * magnitude;

                _camera.transform.localPosition = originalPosition + new Vector3(x, y);
            }

            timeSinceLastShake += Time.deltaTime;
            timeElapsed += Time.deltaTime;

            yield return 0;
        }

        _camera.transform.localPosition = originalPosition;
    }
}
