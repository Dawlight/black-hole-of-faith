﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
public class CameraDebugDrawer : MonoBehaviour
{
    public float Left { get { return transform.position.x - (_cameraSize * _aspectRatio); } }
    public float Right { get { return transform.position.x + (_cameraSize * _aspectRatio); } }
    public float Top { get { return transform.position.y + _cameraSize; } }
    public float Bottom { get { return transform.position.y - _cameraSize; } }

    private float _aspectRatio;
    private float _cameraSize;

    [SerializeField] private Camera _camera;

    void Start()
    {
        _camera = GetComponent<Camera>();   
    }

    void Update()
    {

        _cameraSize = _camera.orthographicSize;
        _aspectRatio = (float)Screen.width / (float)Screen.height;

        var t = transform;

    }
}
