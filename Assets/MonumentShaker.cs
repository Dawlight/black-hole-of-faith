﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonumentShaker : MonoBehaviour
{

    public void ShakeMonument(float duration, float frequency, float magnitude)
    {
        StartCoroutine(DoShakeMonument(duration, frequency, magnitude));
    }

    private IEnumerator DoShakeMonument(float duration, float frequency, float magnitude)
    {
        var originalPosition = transform.localPosition;

        var timeElapsed = 0f;
        var timeSinceLastShake = 0f;
        var timeBetweenShakes = 1f / frequency;

        while (timeElapsed < duration)
        {
            while (timeSinceLastShake > timeBetweenShakes)
            {
                timeSinceLastShake -= timeBetweenShakes;

                var x = Random.Range(-1f, 1f) * magnitude;
                var y = Random.Range(-1f, 1f) * magnitude;

                transform.localPosition = originalPosition + new Vector3(x, y);
            }

            timeSinceLastShake += Time.deltaTime;
            timeElapsed += Time.deltaTime;

            yield return 0;
        }

        transform.localPosition = originalPosition;
    }
}
