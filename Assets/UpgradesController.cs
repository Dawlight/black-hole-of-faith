﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradesController : Singleton<UpgradesController>
{
    public delegate void OnCitizenSpawnRateUpgraded(int level);
    public delegate void OnMaxCitizenUpgraded(int level);
    public delegate void OnSlingMagnitudeRateUpgraded(int level);
    public delegate void OnUpgrade();

    public OnCitizenSpawnRateUpgraded onCitizenSpawnRateUpgraded;
    public OnMaxCitizenUpgraded onMaxCitizenUpgraded;
    public OnSlingMagnitudeRateUpgraded onSlingMagnitudeRateUpgraded;
    public OnUpgrade onUpgrade;

    public int CitizenSpawnRateUpgradeLevel = 0;
    public int MaxCitizensUpgradeLevel = 0;
    public int SlingMagnitudeUpgradeLevel = 0;

    public UpgradeConfiguration Configuration;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    public bool TryUpgradeCitizenSpawnRate()
    {
        if (CitizenSpawnRateUpgradeLevel == Configuration.CitizenSpawnRate.Levels.Length - 1) return false;

        CitizenSpawnRateUpgradeLevel++;
        var upgrade = new CitizenSpawnRateUpgrade(CitizenSpawnRateUpgradeLevel);
        upgrade.Apply(Game.Instance);
        onCitizenSpawnRateUpgraded?.Invoke(CitizenSpawnRateUpgradeLevel);


        onUpgrade?.Invoke();
        return true;
    }

    public bool TryUpgradeMaxCitizens()
    {
        MaxCitizensUpgradeLevel++;
        var upgrade = new MaxCitizensUpgrade(MaxCitizensUpgradeLevel);
        upgrade.Apply(Game.Instance);
        onMaxCitizenUpgraded?.Invoke(MaxCitizensUpgradeLevel);


        onUpgrade?.Invoke();
        return true;
    }

    public bool TryUpgradeSlingMagnitude()
    {
        SlingMagnitudeUpgradeLevel++;
        var upgrade = new SlingMagnitudeUpgrade(SlingMagnitudeUpgradeLevel);
        upgrade.Apply(Game.Instance);
        onSlingMagnitudeRateUpgraded?.Invoke(SlingMagnitudeUpgradeLevel);

        onUpgrade?.Invoke();
        return true;
    }

    public bool IsSpawnRateMaxed() => CitizenSpawnRateUpgradeLevel >= Configuration.CitizenSpawnRate.Levels.Length - 1;

    public bool IsMaxCitizensMaxed() => MaxCitizensUpgradeLevel >= Configuration.MaxCitizens.Levels.Length - 1;

    public bool IsSlingMagnitudeMaxed() => SlingMagnitudeUpgradeLevel >= Configuration.SlingMagnitude.Levels.Length - 1;
}

[Serializable]
public struct UpgradeConfiguration
{
    public MaxCitizensUpgradeConfiguration MaxCitizens;
    public CitizensSpawnRateUpgradeConfiguration CitizenSpawnRate;
    public SlingMagnitudeUpgradeConfiguration SlingMagnitude;
}


[Serializable]
public struct SlingMagnitudeUpgradeConfiguration
{
    public float[] Levels;
}
[Serializable]
public struct MaxCitizensUpgradeConfiguration
{
    public int[] Levels;
}

[Serializable]
public struct CitizensSpawnRateUpgradeConfiguration
{
    public float[] Levels;
}

