﻿Shader "Black Hole/BlobEffect"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _GlobalBlobTexture ("Blob Texture", 2D) = "defaulttexture" {}
        _Contrast ("Contrast", Range(0, 1)) = 0.5
        _Brightness ("Brightness", Range(0, 1)) = 0
        _CutoffMax ("Cutoff Max", Range(0, 1)) = 1
        _CutoffMin ("Cutoff Min", Range(0, 1)) = 0

    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            sampler2D _GlobalBlobTexture;
            float _Contrast;
            float _Brightness;
            float _CutoffMin;
            float _CutoffMax;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 compositeColor;
                fixed4 sceneColor = tex2D(_MainTex, i.uv);
                fixed4 invertedSceneColor = 1 - sceneColor;
                fixed4 blobColor = tex2D(_GlobalBlobTexture, i.uv);
            

                float4 ajustedBlob = (blobColor - 0.5) * _Contrast + 0.5 + _Brightness;  
                float4 clampedBlob = smoothstep(_CutoffMin, _CutoffMax, ajustedBlob);


                return float4(0,0,0,1) * clampedBlob.a + sceneColor * (1 - clampedBlob.a);
            }
            ENDCG
        }
    }
}
