﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonumentHealthBar : MonoBehaviour
{
    public Image MaxHealthIndicator;
    public Image CurrentHealthIndicator;

    private float _maxHealthHeight;
    private Rect _currentHealthRect;

    void Start()
    {
        _maxHealthHeight = MaxHealthIndicator.rectTransform.rect.height;
        _currentHealthRect = CurrentHealthIndicator.rectTransform.rect;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetCurrentHealthFactor(float health)
    {
        health = Mathf.Clamp(health, 0f, 1f);
        var size = CurrentHealthIndicator.rectTransform.sizeDelta;
        size.y = _maxHealthHeight * health;
        CurrentHealthIndicator.rectTransform.sizeDelta = size;
    }
}
